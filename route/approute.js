// const jwt = require('jsonwebtoken');
const authMiddleware = require('../middleware/jwt');
module.exports = function(app) {
  var ContentDelivery = require('../Module/ContentDelivery/controller/Level');
  var Sing = require('../Module/Sing/controller/Sing');
  var User = require('../Module/User/controller/User');
  var Contest = require('../Module/Contest/controller/Contest');
  var Concept = require("../Module/Curriculam/controller/Concept");
  var notifier = require("../Module/Notifier/controller/Notifier");
  var feed  = require("../Module/Watch/controller/Feed");
  var teacher = require("../Module/Teacher/controller/Teacher");
  var Carousel = require("../Module/Walkthrough/controller/Carousel");


  
  app.route('/v1.0/contentdelivery/exercise/:ExerciseId').get([
    authMiddleware.validJWTNeeded,
    ContentDelivery.getLevelByExerciseId
  ])
  app.route('/v1.0/sing/song/trending').get([
    authMiddleware.validJWTNeeded,
    Sing.getTrendingSong
  ])
  app.route('/v1.0/sing/song/getURL/:songId').get([
    authMiddleware.validJWTNeeded,
    Sing.getSongURL
  ])
  app.route('/v1.0/verify/user/:token').get([
    User.getUserByToken
  ])
  app.route('/v1.0/user/login/streak').get([
    authMiddleware.validJWTNeeded,
    User.createUserLoginStreaks
  ])
  app.route('/v1.0/user/login/get-streak').get([
    authMiddleware.validJWTNeeded,
    User.getUserLoginStreaks
  ])
  app.route('/v1.0/contest').post([
    authMiddleware.validJWTNeeded,
    Contest.createContest
  ])
  app.route('/v1.0/contest').get([
    authMiddleware.validJWTNeeded,
    Contest.listAllItem
  ])
  app.route('/v1.0/contest/:contestId').get([
    authMiddleware.validJWTNeeded,
    Contest.listItemById
  ])
  app.route('/v1.0/contest/:contestId').delete([
    authMiddleware.validJWTNeeded,
    Contest.deleteItemById
  ])
  app.route('/v1.0/user').get([
    authMiddleware.validJWTNeeded,
    User.getUserInfo
  ])
  app.route('/v1.0/user/oneReupeeSubscription/create-code/:totalCode').get([
    authMiddleware.validJWTNeeded,
    User.generateSubscriptionCode
  ])
  app.route('/v1.0/user/oneReupeeSubscription/assign-user/:code').get([
    authMiddleware.validJWTNeeded,
    User.assignUser
  ])
  app.route('/v1.0/user/oneReupeeSubscription/get-code').get([
    authMiddleware.validJWTNeeded,
    User.getCode
  ])
  app.route('/v1.0/curriculam/lessonByConcept/:conceptId').get([
    authMiddleware.validJWTNeeded,
    Concept.getLessonByConceptId
  ])
  app.route('/v1.0/curriculam/TuneLessonByConcept/:conceptId').get([
    authMiddleware.validJWTNeeded,
    Concept.getTuneLessonByConceptId
  ])
  app.route('/v1.0/curriculam/TimingLessonByConcept/:conceptId').get([
    authMiddleware.validJWTNeeded,
    Concept.getTimingLessonByConceptId
  ])
  app.route('/v1.0/user/userDelete').delete([
    authMiddleware.validJWTNeeded,
    User.userDelete
  ])
  app.route('/v1.0/user/userSubscribeList').get([
    authMiddleware.validJWTNeeded,
    User.userSubscribeList
  ])
  app.route('/v1.0/user/userSubscriptionList').get([
    authMiddleware.validJWTNeeded,
    User.userSubscriptionList
  ])
  app.route('/v1.0/user/saveLesson').post([
    authMiddleware.validJWTNeeded,
    User.saveLesson
  ])
  app.route('/v1.0/notifier/list').get([
    authMiddleware.validJWTNeeded,
    notifier.notificationList
  ])
  app.route('/v1.0/watch/feed/following').get([
    authMiddleware.validJWTNeeded,
    feed.getFollowingFeed
  ])
  app.route('/v1.0/teacher/details/:teacherId').get([
    authMiddleware.validJWTNeeded,
    teacher.getTeacherDetails
  ])
  app.route('/v1.0/walkthrough/carousel/delete').delete([
    //authMiddleware.validJWTNeeded,
    Carousel.deleteCarousel
  ])
  app.route('/v1.0/cover/submits').get([
    authMiddleware.validJWTNeeded,
    feed.getCoverSubmitedByUser
  ])
  app.route('/v1.0/watch/comment/reply').post([
    authMiddleware.validJWTNeeded,
    feed.AddReply
  ])
  app.route('/v1.0/watch/comment/:CommentId').get([
    authMiddleware.validJWTNeeded,
    feed.GetCommentById
  ])
  app.route('/v1.0/curriculam/UpdateVideo').get([
    //authMiddleware.validJWTNeeded,
    Concept.UpdateVideoById
  ])
  app.route('/v1.0/watch/draft').post([
    authMiddleware.validJWTNeeded,
    feed.GetDraft
  ])

  app.route('/v1.0/watch/download-video').post([
    // authMiddleware.validJWTNeeded,
    feed.downloadVideo
  ])
  
  app.route('/v1.0/watch/add/comment').post([
    authMiddleware.validJWTNeeded,
    feed.AddComment
  ])

  app.route('/v1.0/user/signUp').post([    
    User.signUp
  ])

  app.route('/v1.0/user/existence').post([
    User.existence
  ])

  app.route('/v1.0/user/createHandle').post([
    User.createHandle
  ])

  app.route('/v1.0/user/get-s3-Object').post([
    feed.getS3Object
  ])

  app.route('/v1.0/contest/entry').post([
    authMiddleware.validJWTNeeded,
    Contest.createContestEntry
  ])
  // app.route('/v1.0/mark/attempt/contest').post([
  //   authMiddleware.validJWTNeeded,
  //   Contest.markAttemptAsContest
  // ])
  app.route('/v1.0/contest-entry/attempt/list').post([
   // authMiddleware.validJWTNeeded,
    Contest.entryAttemptList
  ])
  app.route('/v1.0/contest-entry/vote').post([
    authMiddleware.validJWTNeeded,
    Contest.contestVote
  ])
  app.route('/v1.0/contest-entry/mark-winner').post([
    authMiddleware.validJWTNeeded,
    Contest.markWinner
  ])
  app.route('/v1.0/contest-entry/mark-invalid/:id').get([
    authMiddleware.validJWTNeeded,
    Contest.markInvalid
  ])
  app.route('/v1.0/teacher/createTeacher/:userId').post([
    authMiddleware.validJWTNeeded,
    teacher.createTeacher
  ])

  app.route('/v1.0/user/teacher/check').get([
    authMiddleware.validJWTNeeded,
    teacher.checkTeacher
  ])

  app.route('/v1.0/watch/update-media-url-for-staging-only').post([
    authMiddleware.validJWTNeeded,
    feed.updateCoverMediaUrlStatusForStaging
  ])

  
  app.route('/v1.0/contest/upload/get-upload-url').get([
    authMiddleware.validJWTNeeded,
    Contest.getPutObjectUrl
  ])

  app.route('/v1.0/teacher/attribute/update').put([
    authMiddleware.validJWTNeeded,
    teacher.updateAttribute
  ])

  app.route('/v1.0/contest').delete([
    authMiddleware.validJWTNeeded,
    Contest.deleteById
  ])

  app.route('/v1.0/user/follow').post([
    authMiddleware.validJWTNeeded,
    User.userFollow
  ])

  app.route('/v1.0/user/search').get([
    //authMiddleware.validJWTNeeded,
    User.search
  ])

  app.route('/v1.0/user/list').get([
    authMiddleware.validJWTNeeded,
    User.list
  ])

  app.route('/v1.0/contest/deleteEntry/:attemptId').delete([
    authMiddleware.validJWTNeeded,
    Contest.entryDelete
  ])
};




    
