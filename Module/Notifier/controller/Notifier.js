var notifier = require("../model/notifier");

exports.notificationList = function(req, res) {
    notifier.getNotificationList(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};
