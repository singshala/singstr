"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");

// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.url);
const NotificationSchema = require("../../../Schema/NotificationSchema");
const OnEventNotificationSchema = require("../../../Schema/OnEventNotificationSchema");


let notifier = {};
const NotificationModel = conUser.model("NOTIFICATION", NotificationSchema, "Notification");
const OnEventNotificationModel = conUser.model("ONEVENTNOTIFICATION", OnEventNotificationSchema, "OnEventNotification");
 


notifier.getNotificationList = (req) => {
    let page_token = (req.query.page_token) ? req.query.page_token : ""
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    return new Promise((resolve, reject) => {
        (async function() {
            let query=""
            let allEvent = await GetEvent();
            let getDailyChallengeObj = allEvent.find((obj) => obj.on_events[0].name == "DailyChallenge");
            if(getDailyChallengeObj != undefined && getDailyChallengeObj != null && Object.keys(getDailyChallengeObj).length > 0 ){
                query = {
                    user_ids : userId,
                    //on_event_notification_id : [{$ne : ""},{$ne : getDailyChallengeObj._id}]
                    $and : [{on_event_notification_id : {$ne : ""}},{on_event_notification_id:{$ne:getDailyChallengeObj._id}}]
                }
            }else{
                query = {
                    user_id : userId
                }
            }
            if(page_token != ""){
                query._id = {$lt : page_token}
            }
            NotificationModel.distinct("data").find(query).sort({created_at:-1}).limit(10).lean().exec(
                function(err,result){
                    if(err){
                        reject(err)
                    }else{
                        if(result.length > 0){
                            let next_page_token = result[result.length-1]._id;
                            const finalData = []
                            for(let i = 0; i< result.length; i++){
                                let obj = result[i];
                                let eventData = allEvent.find((event) => event._id == obj.on_event_notification_id);
                                if(eventData != undefined && eventData != null && Object.keys(eventData).length > 0){
                                    let eventName = (eventData != undefined && eventData != null && Object.keys(eventData).length > 0 ) ? eventData.on_events[0].name: "";
                                    let message = obj.data.toString('ascii');
                                    let newObj = {
                                        notification_id : obj._id,
                                        title : obj.subject,
                                        message : message,
                                        notification_event_type : eventName,
                                        notification_type_image_url : obj.icon_url,
                                        seen : obj.seen,
                                        time: obj.created_at,
                                        deep_link : obj.deep_link
                                    }
                                    finalData.push(newObj)
                                
                                }
                            }
                                   
                            //let dataArr = [];
                            let dataObj = {
                                data : (finalData.length > 0) ? finalData : null,
                                next_page_token :  (finalData.length > 0) ? next_page_token : ""
                            }
                            //dataArr.push(dataObj)
                            resolve(dataObj)
                        
                        }else{
                            resolve([])
                        }
                    }
                }
            )
        })()
    })

}

const GetEvent = () => {
    return new Promise((resolve, reject) => {
        OnEventNotificationModel.find().select({on_events : 1}).exec(
          function(err,result){
            if(err){
              reject(err)
            }else{
              if(result.length > 0){
                resolve(result)
              }else{
                resolve([])
              }
            }
          })
      })
}
module.exports = notifier;