'user strict';
const mongoose = require("mongoose");
const config = require('../config.json');

mongoose.Promises = global.Promises;
mongoose.set('debug', true)
let conLevel = mongoose.createConnection(config.mongo.url);
const LevelSchema = require("../../../Schema/LevelSchema");


let Level = {}
const LevelModel = conLevel.model('LEVEL',LevelSchema,'Level');
Level.getLevelByExerciseId  = (ExerciseId) => { 
    // console.log("ExerciseId=======",ExerciseId)
    return new Promise((resolve, reject) => {
        LevelModel.find(
            {
                lessons: {
                    $elemMatch: {
                        exercises : {
                            $elemMatch: {
                                _id: ExerciseId
                            }
                        }
                    }
                }
            }
        ).exec(async function (err, result) {
            console.log("ERR========",err)
            if (err) {
              reject(err);
            } else {
              resolve(result);
              mongoose.connection.close();
            }
        });
    })      
}    

module.exports= Level;
