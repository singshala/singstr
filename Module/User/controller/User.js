var User = require('../model/User');
var LoginStreaks = require('../model/LoginStreaks');
var OneRupeeSubscription = require("../model/OneRupeeSubscription");
var UserLesson = require("../model/UserLesson");
var UserDelete = require("../model/UserDelete");
exports.getUserByToken = function(req, res) {
    User.getUserByToken(req.params.token)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.createUserLoginStreaks = function(req, res) {
    LoginStreaks.createUserLoginStreaks(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getUserLoginStreaks = function(req, res) {
    LoginStreaks.getUserLoginStreaks(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getUserInfo = function(req, res) {
    User.getUserInfo(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.generateSubscriptionCode = function(req,res) {
    OneRupeeSubscription.generateSubscriptionCode(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.assignUser = function(req,res) {
    OneRupeeSubscription.assignUser(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getCode = function(req,res) {
    OneRupeeSubscription.getCode(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
}

exports.userDelete = function(req,res) {
    UserDelete.delete(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
}

exports.userSubscribeList = function(req,res) {
    User.userSubscribeList(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
}

exports.userSubscriptionList = function(req,res) {
    User.userSubscriptionList(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
}

exports.saveLesson = function(req,res) {
    UserLesson.saveLesson(req,res)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
}

exports.signUp = function(req, res) {
    User.signUp(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.existence = function(req, res) {
    User.existence(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.createHandle = function(req, res) {
    User.createHandle(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.userFollow = function(req, res) {
    User.userFollow(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.search = function(req, res) {
    User.search(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};


exports.list = function(req, res) {
    User.list(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};








