"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");


mongoose.set("debug", true);
//let conUser = mongoose.createConnection(config.mongo.url+'replicaSet=testrep');
let conUser = mongoose.createConnection(config.mongo.url);
let conAward = mongoose.createConnection(config.mongo.awardUrl);
let conCurriculum = mongoose.createConnection(config.mongo.curriculumUrl);
let conWatch = mongoose.createConnection(config.mongo.watchUrl);
let conSing = mongoose.createConnection(config.mongo.singUrl);
let conNotifier = mongoose.createConnection(config.mongo.notifierUrl);
let consingStrTopTen = mongoose.createConnection(config.mongo.singStrTopTenUrl);
let collectionArr = [];
//collection list of singStrTopTen Db

 
const AnswerHistoryRepoSchema = require("../../../Schema/AnswerHistoryRepoSchema");
const AttemptSchema = require("../../../Schema/AttemptSchema");
const AttemptUploadSchema = require("../../../Schema/AttemptUploadSchema");
const AwardXpLeaderSchema = require("../../../Schema/AwardXpLeaderSchema");
const AwardXpSchema = require("../../../Schema/AwardXpSchema");
const ClapHistorySchema = require("../../../Schema/ClapHistorySchema");
const CommentHistorySchema = require("../../../Schema/CommentHistorySchema");
const CommentSchema = require("../../../Schema/CommentSchema");
const CoverWatchHistorySchema = require("../../../Schema/CoverWatchHistorySchema");
const DraftXpSchema = require("../../../Schema/DraftXpSchema");
const ExcerciseComppletedSchema = require("../../../Schema/ExcerciseComppletedSchema");
const HistorySchema = require("../../../Schema/HistorySchema");
const LevelWatchedSchema = require("../../../Schema/LevelWatchedSchema");
const OnEventNotificationHistorySchema = require("../../../Schema/OnEventNotificationHistorySchema");
const OneRupeeSubscriberSchema = require("../../../Schema/OneRupeeSubscriberSchema");
const SingReviewSchema = require("../../../Schema/SingReviewSchema");
const UnSubsriberHistorySchema = require("../../../Schema/UnSubsriberHistorySchema");
const UserAccountTransactionSchema = require("../../../Schema/UserAccountTransactionSchema");
const UserLessonSchema = require("../../../Schema/UserLessonSchema");
const UserLoginStreaksSchema = require("../../../Schema/UserLoginStreaksSchema");
const UserPreferencesSchema = require("../../../Schema/UserPreferencesSchema");
const UserSchema = require("../../../Schema/UserSchema");
const UserAccountSchema = require("../../../Schema/UserAccountSchema");
const UserSubsriberSchema = require("../../../Schema/UserSubsriberSchema");

const UserModel = conUser.model("USER", UserSchema, "User");
const UserAccountModel = conUser.model("USERACCOUNT", UserAccountSchema, "UserAccount");
const UserSubsriberModel = conUser.model("USERSUBSRIBER", UserSubsriberSchema, "UserSubsriber");
const UserPreferences = conUser.model("USERPREFERENCES", UserPreferencesSchema, "UserPreferences");
const UserLoginStreaksModel = conUser.model("USERLOGINSTREAKS",UserLoginStreaksSchema,"UserLoginStreaks");
const UserLessonModel = conUser.model("USERLESSON",UserLessonSchema,"UserLesson");
const UserAccountTransactionModel = conUser.model("USERACCOUNTTRANSACTION",UserAccountTransactionSchema,"UserAccountTransaction");
const UnSubsriberHistoryModel = conUser.model("UNSUBSRIBERHISTORY",UnSubsriberHistorySchema,"UnSubsriberHistory");
const OneRupeeSubscribersModel = conUser.model("ONERUPEESUBSCRIBERS",OneRupeeSubscriberSchema,"OneRupeeSubscribers");

const AwardXpLeaderModel = conAward.model("AWARDXPLEADER",AwardXpLeaderSchema,"AwardXpLeader");
const AwardXpModel = conAward.model("AWARDXP",AwardXpSchema,"AwardXp");
const DraftXpModel = conAward.model("DRAFTXP",DraftXpSchema,"DraftXp");


const AnswerHistoryRepoModel = conCurriculum.model("ANSWERHISTORYREPO",AnswerHistoryRepoSchema,"AnswerHistoryRepo");
const ExcerciseComppletedModel = conCurriculum.model("EXCERCISECOMPPLETED",ExcerciseComppletedSchema,"ExcerciseComppleted");
const LevelWatchedModel = conCurriculum.model("LEVELWATCHED",LevelWatchedSchema,"LevelWatched");

const SingReviewModel = conSing.model("SINGREVIEW",SingReviewSchema,"SingReview");

const OnEventNotificationHistoryModel = conNotifier.model("ONEVENTNOTIFICATIONHISTORY",OnEventNotificationHistorySchema,"OnEventNotificationHistory");

const AttemptModel = conWatch.model("ATTEMPT",AttemptSchema,"Attempt");
const AttemptUploadModel = conWatch.model("ATTEMPTUPLOAD",AttemptUploadSchema,"AttemptUpload");
const ClapHistoryModel = conWatch.model("CLAPHOSTORY",ClapHistorySchema,"ClapHistory");
const CommentHistoryModel = conWatch.model("COMMENTHISTORY",CommentHistorySchema,"CommentHistory");
const CommentModel = conWatch.model("COMMENT",CommentSchema,"Comment");
const CoverWatchHistoryModel = conWatch.model("COVERWATCHHISTORY",CoverWatchHistorySchema,"CoverWatchHistory");
const HistoryModel = conWatch.model("HISTORY",HistorySchema,"History");

let UserDelete = {};


UserDelete.delete = (req) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    // consingStrTopTen.on('open', function () {
    //   consingStrTopTen.db.listCollections().toArray(function (err, collectionsList) {
    //     if (err) {
    //       console.log(err);
    //     } else {
    //       //console.log(collectionsList);
    //       if(collectionsList.length > 0){
    //         collectionArr = collectionsList.map((obj) => obj.name)
    //       }
    //     }
    //     console.log("hello")
    //     return collectionArr;
    //     mongoose.connection.close();
    //   });
    // });
    
   
    return new Promise((resolve, reject) => {
        (async function() {
            let data = await deleteUserData(userId);
            console.log(data)
            if(data == true){
              resolve("data deleted");
            }else{
              reject(data)
            }
            
        })()
    });
  
}

async function deleteUserData(userId) {

  const session = await AwardXpLeaderModel.startSession();
    session.startTransaction();
    try {
      const opts = { session };
      
      const deleteAwardXpLeader = await AwardXpLeaderModel.deleteMany ({ userid: userId }, opts);
      const deleteAwardXp = await AwardXpModel.deleteMany ({ user_id: userId }, opts);
      const deleteDraftdXp = await DraftXpModel.deleteMany ({ user_id: userId }, opts);

      if(collectionArr.length > 0){
        console.log(collectionArr.length)
        for(let i = 0; i< collectionArr.length; i++){
          let collectionName = consingStrTopTen.collection(collectionArr[i]);
          await collectionName.deleteMany({user_id:userId}, opts)
        }
      }

      const deleteAnswerHistoryRepo = await AnswerHistoryRepoModel.deleteMany ({ user_id: userId }, opts);
      const deleteExcerciseCompplete = await ExcerciseComppletedModel.deleteMany ({ user_id: userId }, opts);
      const deleteLevelWatched = await LevelWatchedModel.deleteMany ({ user_id: userId }, opts);

      const deleteOnEventNotificationHistory = await OnEventNotificationHistoryModel.deleteMany ({ user_id: userId }, opts);

      const deleteSingReview = await SingReviewModel.deleteMany ({ user_id: userId }, opts);

      const deleteAttempt = await AttemptModel.deleteMany ({ user_id: userId }, opts);
      const deleteAttemptUpload = await AttemptUploadModel.deleteMany ({ user_id: userId }, opts);
      const deleteClapHistory = await ClapHistoryModel.deleteMany ({ user_id: userId }, opts);
      const deleteCommentHistory = await CommentHistoryModel.deleteMany ({ user_id: userId }, opts);
      const deleteComment = await CommentModel.deleteMany ({ "user.id": userId }, opts);
      const deleteCoverWatchHistory = await CoverWatchHistoryModel.deleteMany ({ user_id: userId }, opts);
      const deleteHistory = await HistoryModel.deleteMany ({ user_id: userId }, opts);

      const deleteOneRupeeSubscribers = await OneRupeeSubscribersModel.deleteMany ({ userId: userId }, opts);
      const deleteUnSubsriberHistory = await UnSubsriberHistoryModel.findOneAndDelete ({ user_id: userId }, opts);
      const deleteUserAccountTransaction = await UserAccountTransactionModel.findOneAndDelete ({ user_id: userId }, opts);
      const deleteUserLesson = await UserLessonModel.deleteMany ({ user_id: userId }, opts);
      const deleteUserLoginStreaks = await UserLoginStreaksModel.deleteMany ({ userId: userId }, opts);
      const deleteUserPreferences = await UserPreferences.deleteMany ({ user_id: userId }, opts);
      const deleteUserSubsriber = await UserSubsriberModel.findOneAndDelete ({ user_id: userId }, opts);
      const deleteUserAccount = await UserAccountModel.findOneAndDelete ({ _id: userId }, opts);
      const deleteUser = await UserModel.findOneAndDelete ({ _id: userId }, opts);

      await session.commitTransaction();
      session.endSession();
      return true;
    } catch (error) {
      // If an error occurred, abort the whole transaction and
      // undo any changes that might have happened
      await session.abortTransaction();
      session.endSession();
      return error; 
      
    }
  }

  module.exports = UserDelete;