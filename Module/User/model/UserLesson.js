"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");


// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.url);
const UserLessonSchema = require("../../../Schema/UserLessonSchema");


let UserLesson = {};
const UserLessonModel = conUser.model(
  "USERLESSON",
  UserLessonSchema,
  "UserLesson"
);

UserLesson.saveLesson = (req,res) => {
    let lessonArr = req.body.lesson_ids;
    let error = "";
        let status = "";
    if (!lessonArr) {
        return res
          .status(400)
          .send({ status: 400, message: "lesson ids  required!!" });
      }
     
      else if (lessonArr.length == 0) {
        return res
          .status(400)
          .send({ status: 400, message: "lesson ids cannot be an empty array!!" });
      }else{
        
   
        let authorization = req.headers["authorization"].split(" ");
        var decoded = jwt_decode(authorization[1]);
        let userId = decoded.user_id;
        console.log(userId)
        return new Promise((resolve, reject) => {
            for(let i = 0; i <  lessonArr.length; i++){
                let data = { user_id: userId, lesson_id: lessonArr[i], time : new Date()};
                UserLessonModel.find({user_id: userId, lesson_id: lessonArr[i]}).exec(
                    function(err,result){
                        if(err){
                            error = err
                        }else{
                            if(result.length == 0){ 
                                UserLessonModel.collection.insertOne(data,
                                    function (err, res) {
                                        if (err) {
                                            error = err
                                        }else{
                                            status = "Record inserted";
                                        }
                                    }
                                )
                            }else{
                                status = "already present";
                            }
                        }
                    }
                )
                
  
            }
            if (error) {
                reject(error);
            } else {
                resolve("Record inserted");
            }
        })
    }
}
module.exports = UserLesson;