const mongoose = require("mongoose");
const config = require("../config.json");
const { v4: uuidv4 } = require('uuid');
let jwt_decode = require("jwt-decode");


mongoose.set("debug", true);

let conUser = mongoose.createConnection(config.mongo.url);
const OneRupeeCodeSchema = require("../../../Schema/OneRupeeCodeSchema");
const OneRupeeSubscriberSchema = require("../../../Schema/OneRupeeSubscriberSchema");
const UserAccountSchema = require("../../../Schema/UserAccountSchema");

const OneRupeeCodeModel = conUser.model(
    "ONERUPEECODES",
    OneRupeeCodeSchema,
    "OneRupeeCodes"
  );

const OneRupeeSubscriberModel = conUser.model(
    "ONERUPEESUBSCRIBERS",
    OneRupeeSubscriberSchema,
    "OneRupeeSubscribers"
  );

const UserAccountModel = conUser.model(
    "USERACCOUNT",
    UserAccountSchema,
    "UserAccount"
  );

exports.generateSubscriptionCode = (req) => {
    const totalReqCode = (req.params.totalCode) ? req.params.totalCode : 1;
    let error = "";
    return new Promise((resolve, reject) => {
        for(let i = 0; i < totalReqCode; i++){
            const uuid = uuidv4();
            let insertObj = {
                code : uuid,
                createdAt : new Date()
            }
            OneRupeeCodeModel.collection.insertOne(insertObj, function (err, response) {
                if(err){
                    error = err;
                }
            });
        }
        if (error) {
          reject(error);
        } else {
          resolve("Record inserted");
        }
    });
}

exports.assignUser = (req,res) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    const code = req.params.code;
    return new Promise((resolve, reject) => {
        OneRupeeSubscriberModel.find({userId : userId}).exec(
            function(err,result) {
                if(err){
                    throw err;
                }else{
                    if(result.length > 0){
                        resolve("user already redeem a code")
                    }else{
                        OneRupeeCodeModel.find({code : code}).exec( //get code id
                            function(err,codeResult){
                                if(err){
                                    throw err
                                }else{
                                    if(codeResult.length > 0){
                                        const codeId = codeResult[0]._id.toHexString();
                                        let insertObj = {
                                            _id : codeId,
                                            userId : userId,
                                            assign : 1,
                                            createdAt : new Date()
                                        }
                                        OneRupeeSubscriberModel.find({_id: codeId}).exec( //for checking code is already present or not
                                            function (err, result) {
                                                if (err) {
                                                  throw err;
                                                }else{
                                                    if(result.length > 0){
                                                        resolve("code already used")
                                                    }else{
                                                        OneRupeeSubscriberModel.collection.insertOne(insertObj, function (err, response) {
                                                            if(err){
                                                                reject(err)
                                                            }else{
                                                                //code for extend 1 month subscription 
                                                                const newValidity = new Date();
                                                                newValidity.setDate(newValidity.getDate() + 30);
                                                                UserAccountModel.findOneAndUpdate({_id : userId}, 
                                                                    {
                                                                        $set: {
                                                                            subscription: {
                                                                                validity : newValidity
                                                                            }
                                                                        }
                                                                    }, {new : true} , 
                                                                    function(err,data){
                                                                        if(err){
                                                                            reject(err)
                                                                        }else{
                                                                            resolve("inserted and subscription extended")
                                                                        }
                                                                    }
                                                                );
                                                            }
                                                        });
                                                    }
                                                }
                                            }
                                        );
                                    }else{
                                        resolve("code not found")
                                    }
                                }
                            }
                        )
                    }
                }
            }
        );
    });
}


exports.getCode = (req,res) => {
    return new Promise((resolve, reject) => {
        OneRupeeCodeModel.find().exec(
            function (err, result) {
                if (err) {
                  throw err;
                }else{
                    if(err){
                        reject(err)
                    }else{
                        const finalResult = result.map((item) => {
                            item._id = item._id.toHexString();
                            return item
                        })
                       
                        //return this._id.toHexString();
                        resolve(finalResult)
                    }
               }
            }
        );
    });
}