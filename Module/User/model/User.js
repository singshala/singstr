"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");
const jwt = require("jsonwebtoken");

// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.url);
let conNotifierUrl = mongoose.createConnection(config.mongo.notifierUrl);
const UserSchema = require("../../../Schema/UserSchema");
const UserAccountSchema = require("../../../Schema/UserAccountSchema");
const UserSubsriberSchema = require("../../../Schema/UserSubsriberSchema");
let NotificationSchema = require("../../../Schema/NotificationSchema");
const OnEventNotificationSchema = require("../../../Schema/OnEventNotificationSchema");

let User = {};
const UserModel = conUser.model("USER", UserSchema, "User");
const UserAccountModel = conUser.model(
  "USERACCOUNT",
  UserAccountSchema,
  "UserAccount"
);

const UserSubsriberModel = conUser.model(
  "USERSUBSRIBER",
  UserSubsriberSchema,
  "UserSubsriber"
);

const NotificationModel = conNotifierUrl.model(
  "NOTIFICATION",
  NotificationSchema,
  "Notification"
);
const OnEventNotificationModel = conNotifierUrl.model(
  "ONEVENTNOTIFICATION",
  OnEventNotificationSchema,
  "OnEventNotification"
);

User.getUserByToken = (token) => {
  return new Promise((resolve, reject) => {
    UserModel.find({
      devices: {
        $elemMatch: {
          access_token: token,
        },
      },
    }).exec(async function (err, result) {
      if (err) {
        reject(err);
      } else {
        if (result.length > 0) {
          resolve(result);
        } else {
          reject(err);
        }
      }
    });
  });
};

User.getUserInfo = (req) => {
  return new Promise((resolve, reject) => {
    let token = req.headers["authorization"].split(" ");
    UserModel.find({
      devices: {
        $elemMatch: {
          access_token: token[1],
        },
      },
    })
      .lean()
      .exec(async function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            // console.log(result[0]._id);
            UserAccountModel.find({
              _id: result[0]._id,
            })
              .lean()
              .exec(async function (errAccount, resultAccount) {
                if (errAccount) {
                  reject(errAccount);
                } else {
                  result[0].accountInfo = resultAccount[0];
                  result[0].accountInfo.stats.number_of_cover = resultAccount[0]
                    .stats.number_of_cover
                    ? resultAccount[0].stats.number_of_cover
                    : 0;
                  result[0].accountInfo.stats.level = resultAccount[0].stats
                    .level
                    ? resultAccount[0].stats.level
                    : 0;
                  result[0].accountInfo.stats.nextlevel = resultAccount[0].stats
                    .nextlevel
                    ? resultAccount[0].stats.nextlevel
                    : 0;
                  result[0].accountInfo.stats.xp = resultAccount[0].stats.xp
                    ? resultAccount[0].stats.xp
                    : 0;
                  result[0].accountInfo.stats.remaining_next_xp =
                    resultAccount[0].stats.remaining_next_xp
                      ? resultAccount[0].stats.remaining_next_xp
                      : 0;
                  result[0].accountInfo.stats.avg_tune = resultAccount[0].stats
                    .avg_tune
                    ? resultAccount[0].stats.avg_tune
                    : 0;
                  result[0].accountInfo.stats.avg_time = resultAccount[0].stats
                    .avg_time
                    ? resultAccount[0].stats.avg_time
                    : 0;
                  result[0].accountInfo.stats.remaining_practice_time =
                    resultAccount[0].stats.remaining_practice_time
                      ? resultAccount[0].stats.remaining_practice_time
                      : 0;
                  result[0].accountInfo.stats.subscribe = resultAccount[0].stats
                    .subscribe
                    ? resultAccount[0].stats.subscribe
                    : 0;
                  result[0].accountInfo.stats.subscription = resultAccount[0]
                    .stats.subscription
                    ? resultAccount[0].stats.subscription
                    : 0;
                  result[0].accountInfo.stats.status = resultAccount[0].stats
                    .status
                    ? resultAccount[0].stats.status
                    : false;
                  result[0].accountInfo.stats.draftCount = resultAccount[0]
                    .stats.draftCount
                    ? resultAccount[0].stats.draftCount
                    : 0;
                  result[0].accountInfo.stats.pendingxp = resultAccount[0].stats
                    .pendingxp
                    ? resultAccount[0].stats.pendingxp
                    : 0;
                  resolve(result[0]);
                }
              });
          } else {
            reject(err);
          }
        }
      });
  });
};

// User.userDelete = (req) => {
//   let authorization = req.headers["authorization"].split(" ");
//   var decoded = jwt_decode(authorization[1]);
//   let userId = decoded.user_id;

//   return new Promise((resolve, reject) => {
//     UserModel.findOneAndUpdate({ _id: userId },{ $set: { status : 0 }},
//     { new: true }, function (err, res) {
//       if(err){
//         reject(err)
//       }else{
//         resolve("updated")
//       }
//     });
//   });

// }

User.userSubscribeList = (req) => {
  //following
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  let reqUserId = req.query.userId ? req.query.userId : userId;
  return new Promise((resolve, reject) => {
    UserSubsriberModel.distinct("subscriber_id", {
      user_id: reqUserId,
      subscriber_id: { $ne: reqUserId },
    }).exec(function (err, result) {
      if (err) {
        reject(err);
      } else {
        if (result.length > 0) {
          UserModel.find({ _id: { $in: result } })
            .select({
              first_name: 1,
              last_name: 1,
              email: 1,
              user_handle: 1,
              original_url: 1,
              profile_img: 1,
            })
            .lean()
            .exec(function (err, data) {
              if (err) {
                reject(err);
              } else {
                if (data.length > 0) {
                  (async function () {
                    let subsribeData = await SubscribeList(userId);

                    let finalData = data.map((obj) => {
                      let imageURL = config.aws.s3.cdn_prefix_url;
                      let image = obj.original_url
                        ? imageURL + obj.original_url
                        : obj.profile_img;
                      let subscribe = false;

                      if (subsribeData.indexOf(obj._id) >= 0) {
                        subscribe = true;
                      }
                      return (obj = { ...obj, image, subscribe });
                    });
                    resolve(finalData);
                  })();
                } else {
                  resolve("not found");
                }
              }
            });
        } else {
          resolve([]);
        }
      }
    });
  });
};

User.userSubscriptionList = (req) => {
  //followers
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  let reqUserId = req.query.userId ? req.query.userId : userId;

  return new Promise((resolve, reject) => {
    UserSubsriberModel.distinct("user_id", {
      subscriber_id: reqUserId,
      user_id: { $ne: reqUserId },
    }).exec(function (err, result) {
      if (err) {
        reject(err);
      } else {
        if (result.length > 0) {
          UserModel.find({ _id: { $in: result } })
            .select({
              first_name: 1,
              last_name: 1,
              email: 1,
              user_handle: 1,
              original_url: 1,
              profile_img: 1,
            })
            .lean()
            .exec(function (err, data) {
              if (err) {
                reject(err);
              } else {
                if (data.length > 0) {
                  (async function () {
                    let subsribeData = await SubscribeList(userId);

                    let finalData = data.map((obj) => {
                      let imageURL = config.aws.s3.cdn_prefix_url;
                      let image = obj.original_url
                        ? imageURL + obj.original_url
                        : obj.profile_img;
                      let subscribe = false;

                      if (subsribeData.indexOf(obj._id) >= 0) {
                        subscribe = true;
                      }

                      return (obj = { ...obj, image, subscribe });
                    });

                    resolve(finalData);
                  })();
                } else {
                  resolve("not found");
                }
              }
            });
        } else {
          resolve([]);
        }
      }
    });
  });
};

const SubscribeList = (userId) => {
  return new Promise((resolve, reject) => {
    UserSubsriberModel.distinct("subscriber_id", { user_id: userId }).exec(
      function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            resolve(result);
          } else {
            resolve([]);
          }
        }
      }
    );
  });
};
const SubscriptionList = (userId) => {
  return new Promise((resolve, reject) => {
    UserSubsriberModel.distinct("user_id", { subscriber_id: userId }).exec(
      function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            resolve(result);
          } else {
            resolve([]);
          }
        }
      }
    );
  });
};

User.signUp = (req) => {
  return new Promise((resolve, reject) => {
    //condition login_type 4=facebook, 2=gmail, 3=phone
    //condition login_type 4=Apple, 2=Google 3=facebook 1=Sensibol=Mobile

    if (req.body.login_type == 3) {
      if (!req.body.social_id) {
        reject("Please enter a social_id for FB Login");
      }
      let findCase = {
        social_id: req.body.social_id,
      };
      let findCase1 = {
        user_handle: req.body.user_handle.replace(/\s+/g, '').toLowerCase(),
      };
      UserModel.find(findCase, function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            let devices = [];
            //No need to create JWT token.
            // let token = createJwtToken(response[0]["_id"]);
            // devices = response[0]["devices"];
            // devices.push({
            //   access_token: token,
            // });
            UserModel.findOneAndUpdate(
              { social_id: req.body.social_id },
              {
                $set: {
                  login_type: req.body.login_type,
                  social_id: req.body.social_id,
                  email: req.body.email ? req.body.email : "",
                  password: "",
                  first_name: req.body.first_name,
                  last_name: req.body.last_name,
                  user_handle: req.body.user_handle.replace(/\s+/g, '').toLowerCase(),
                  contact_number: req.body.contact_number
                    ? req.body.contact_number
                    : "",
                  profile_img: req.body.profile_img ? req.body.profile_img : "",
                  status: 1,
                  devices: devices,
                  display_name: req.body.display_name
                    ? req.body.display_name
                    : "",
                  sex:
                    req.body.sex.toLowerCase() == "male"
                      ? 1
                      : req.body.sex.toLowerCase() == "female"
                      ? 2
                      : 0,
                  onboard_status: true,
                  singer_type: req.body.singerType,
                  isTeacher: false,
                },
              },
              { new: true },
              function (err, data) {
                if (err) {
                  reject(err);
                } else {
                  //resolve("User updated");
                  resolve(data);
                }
              }
            );
          } else {
            UserModel.find(findCase1, function (err, response) {
              if (err) {
                reject(err);
              } else {
                if (response.length > 0) {
                  reject("@handle not available. Please use different @handle");
                } else {
                  let id = new mongoose.Types.ObjectId();
                  let devices = [];

                  // let token = createJwtToken(id.toString());
                  // devices.push({
                  //   access_token: token,
                  // });
                  let insertObj = {
                    _id: id.toString(),
                    login_type: req.body.login_type,
                    social_id: req.body.social_id,
                    email: req.body.email ? req.body.email : "",
                    password: "",
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    user_handle: req.body.user_handle.replace(/\s+/g, '').toLowerCase(),
                    contact_number: req.body.contact_number
                      ? req.body.contact_number
                      : "",
                    profile_img: req.body.profile_img
                      ? req.body.profile_img
                      : "",
                    extra: {},
                    status: 1,
                    devices: devices,
                    display_name: req.body.display_name
                      ? req.body.display_name
                      : "",
                    sex:
                      req.body.sex.toLowerCase() == "male"
                        ? 1
                        : req.body.sex.toLowerCase() == "female"
                        ? 2
                        : 0,
                    onboard_status: true,
                    singer_type: req.body.singerType,
                    isTeacher: false,
                  };
                  UserModel.collection.insertOne(
                    insertObj,
                    function (err, response) {
                      if (err) {
                        reject(err);
                      } else {
                        //resolve("Record inserted");
                        resolve(insertObj);
                      }
                    }
                  );
                }
              }
            });
          }
        }
      });
    }
    if (req.body.login_type == 1) {
      if (!req.body.contact_number) {
        reject("Please enter contact_number");
      }
      let findCase = {
        contact_number: req.body.contact_number,
      };
      let findCase1 = {
        user_handle: req.body.user_handle.replace(/\s+/g, '').toLowerCase(),
      };
      //console.log(findCase1)
      UserModel.find(findCase, function (err, response) {
        if (err) {
          reject(err);
        } else {
          // console.log("response==", response);
          if (response.length > 0) {
            reject(
              "User with this mobile number already exists. Please login to continue."
            );
          } else {
            UserModel.find(findCase1, function (err, response) {
              if (err) {
                reject(err);
              } else {
                if (response.length > 0) {
                  reject("@handle not available. Please use different @handle");
                } else {
                  let id = new mongoose.Types.ObjectId();
                  //let token = createJwtToken(id.toString());
                  let devices = [];
                  // devices.push({
                  //   access_token: token,
                  // });
                  let insertObj = {
                    _id: id.toString(),
                    login_type: req.body.login_type,
                    social_id: req.body.contact_number,
                    email: req.body.email ? req.body.email : "",
                    password: "",
                    first_name: req.body.first_name,
                    last_name: req.body.last_name,
                    user_handle: req.body.user_handle.replace(/\s+/g, '').toLowerCase(),
                    contact_number: req.body.contact_number,
                    profile_img: "",
                    status: 1,
                    devices: devices,
                    display_name: req.body.display_name
                      ? req.body.display_name
                      : "",
                    sex:
                      req.body.sex.toLowerCase() == "male"
                        ? 1
                        : req.body.sex.toLowerCase() == "female"
                        ? 2
                        : 0,
                    singer_type: req.body.singerType,
                    isTeacher: false,
                    onboard_status: true,
                  };
                  UserModel.collection.insertOne(
                    insertObj,
                    function (err, response) {
                      if (err) {
                        reject(err);
                      } else {
                        //resolve("Record inserted");
                        resolve(insertObj);
                      }
                    }
                  );
                }
              }
            });
          }
        }
      });
    }
  });
};

let createJwtToken = (user_id) => {
  const token = jwt.sign({ user_id: user_id }, "10", {
    expiresIn: "2h",
  });
  return token;
};

User.existence = (req) => {
  // let authorization = req.headers["authorization"].split(" ");
  // var decoded = jwt_decode(authorization[1]);
  // let userId = decoded.user_id;

  return new Promise((resolve, reject) => {
    let findcase = {
      $or: [
        { social_id: req.body.check_value },
        { contact_number: req.body.check_value },
        { user_handle: req.body.check_value },
      ],
    };
    UserModel.find(findcase, function (err, response) {
      if (err) {
        reject(err);
      } else {
        if (response.length > 0) {
          let responseNew = {};
          responseNew = {
            user_exists: true,
          };
          resolve(responseNew);
        } else {
          let responseNew = {};
          responseNew = {
            user_exists: false,
          };
          resolve(responseNew);
        }
      }
    });
  });
};

User.createHandle = (req) => {
  return new Promise((resolve, reject) => {
    if (!req.body.first_name) {
      reject("Please enter first_name");
    }
    let findCase = {};
    findCase = {
      user_handle: "@" + req.body.first_name,
    };
    if (req.body.last_name) {
      findCase = {
        user_handle: "@" + req.body.first_name + req.body.last_name,
      };
    }
    (async function () {
      let number = 0;
      let response = await generateUniqeuser_handle(findCase, req, number);
      // console.log("response in main mehtod" + response);
      resolve(response);
    })();
    // let response = generateUniqeuser_handle(findCase,req);
    //     console.log("response in main mehtod"+response);
    //     resolve(response);
  });
};

//recursive function
let generateUniqeuser_handle = (findCase, req, number) => {
  return new Promise((resolve, reject) => {
    UserModel.find(findCase, function (err, response) {
      if (err) {
        // console.log("error=" + err);
        reject(err);
      } else {
        if (response.length > 0) {
          number = number + 1;

          findCase = {
            user_handle: "@" + req.body.first_name + number,
          };

          if (req.body.last_name) {
            findCase = {
              user_handle:
                "@" + req.body.first_name + req.body.last_name + number,
            };
          }
          resolve(generateUniqeuser_handle(findCase, req, number));
          // generateUniqeuser_handle(findCase, req);
        } else {
          let response = {};
          response = {
            user_handle: findCase.user_handle.replace(/\s+/g, '').toLowerCase(),
          };
          resolve(response);
        }
      }
    });
  });
};

User.userFollow = (req) => {
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  // console.log(userId);
  return new Promise((resolve, reject) => {
    (async function () {
      //let userId = req.body.userId;
      let subscriberId = req.body.subscriberId;
      let findcase = { user_id: userId, subscriber_id: subscriberId };
      let UserDetailsdata = await UserDetails(userId);

      UserSubsriberModel.find(findcase, function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            UserSubsriberModel.findOneAndDelete(
              findcase,
              function (err, response) {
                if (err) {
                  reject(err);
                } else {
                  (async function () {
                    let subscribe;
                    let result = await subscriberFind(subscriberId);
                    if (result.length > 0) {
                      //console.log("result:", result);
                      if (result[0].stats.subscribe) {
                        result[0].stats.subscribe--;
                      } else {
                        result[0].stats.subscribe = 0;
                      }
                      const subscriberData = await subscribeSave(result);

                      const subscriptionResult = await subscriptionFind(userId);
                      if (subscriptionResult.length > 0) {
                        // console.log("subscriptionResult:", subscriptionResult);
                        if (subscriptionResult[0].stats.subscription) {
                          subscriptionResult[0].stats.subscription--;
                        } else {
                          subscriptionResult[0].stats.subscription = 0;
                        }
                        const subscriptionData = await SubscriptionSave(
                          subscriptionResult
                        );
                      }
                      let responseNew = {};
                      responseNew = {
                        isSubscribed: false,
                      };
                      resolve(responseNew);
                    } else {
                      reject("subscriberId not found");
                    }
                  })();
                }
              }
            );
          } else {
            let id = new mongoose.Types.ObjectId();
            let insertObj = {
              _id: id.toString(),
              user_id: userId,
              subscriber_id: subscriberId.toString(),
              time: new Date(),
            };
            UserSubsriberModel.collection.insertOne(
              insertObj,
              function (err, response) {
                if (err) {
                  reject(err);
                } else {
                  (async function () {
                    let subscribe;
                    let result = await subscriberFind(subscriberId);
                    if (result.length > 0) {
                      // console.log(result);
                      if (result[0].stats.subscribe) {
                        result[0].stats.subscribe++;
                      } else {
                        result[0].stats.subscribe = 1;
                      }
                      const subscriberData = await subscribeSave(result);

                      const subscriptionResult = await subscriptionFind(userId);
                      if (subscriptionResult.length > 0) {
                        //console.log("subscriptionResult:", subscriptionResult);
                        if (subscriptionResult[0].stats.subscription) {
                          subscriptionResult[0].stats.subscription++;
                        } else {
                          subscriptionResult[0].stats.subscription = 1;
                        }
                        const subscriptionData = await SubscriptionSave(
                          subscriptionResult
                        );
                      }

                      (async function () {
                        let GetNotificationEvent = await NotificationEventSave(
                          "Comment"
                        );
                        let notificationMsg = `${UserDetailsdata[0].first_name} ${UserDetailsdata[0].last_name} is following you.`;
                        let notificationData = {
                          userId: subscriberData._id,
                          message: Buffer.from(notificationMsg),
                          notificationEventId: GetNotificationEvent,
                          deep_link:
                            "https://www.singshala.com/app/users/" + userId,
                          icon_url: UserDetailsdata[0].profile_img,
                        };
                        await NotificationSave(notificationData);

                        let responseNew = {};
                        responseNew = {
                          isSubscribed: true,
                        };
                        resolve(responseNew);
                      })();
                    } else {
                      reject("subscriberId not found");
                    }
                  })();
                }
              }
            );
          }
        }
      });
    })();
  });
};

// const UserDetails = (userId) => {
//   return new Promise((resolve, reject) => {
//     UserModel.find({ _id: userId }, function (err, result) {
//       if (err) {
//         reject(err);
//       } else {
//         if (result.length > 0) {
//           resolve(result);
//         } else {
//           resolve([]);
//         }
//       }
//     });
//   });
// };

const UserDetails = (userId) => {
  return new Promise((resolve, reject) => {
    UserModel.find({ _id: userId })
      .select({
        _id: 1,
        first_name: 1,
        last_name: 1,
        profile_img: 1,
        display_name: 1,
        user_handle: 1,
      })
      .exec(function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            resolve(result);
          } else {
            resolve([]);
          }
        }
      });
  });
};

const subscribeSave = (data) => {
  let subscriberId = data[0]._id;
  return new Promise((resolve, reject) => {
    UserAccountModel.findOneAndUpdate(
      { _id: subscriberId },
      { stats: data[0].stats },
      { new: true },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          // console.log("data:", data);
          // console.log("dataresponse:", response);
          resolve(response);
        }
      }
    );
  });
};

const SubscriptionSave = (data) => {
  let userId = data[0]._id;
  return new Promise((resolve, reject) => {
    UserAccountModel.findOneAndUpdate(
      { _id: userId },
      { stats: data[0].stats },
      { new: true },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          // console.log("data:", data);
          // console.log("dataresponse:", response);
          resolve(response);
        }
      }
    );
  });
};

const subscriberFind = (subscriberId) => {
  return new Promise((resolve, reject) => {
    UserAccountModel.find({ _id: subscriberId }, function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
};

const subscriptionFind = (userId) => {
  return new Promise((resolve, reject) => {
    UserAccountModel.find({ _id: userId }, function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
};

const NotificationEventSave = (eventType) => {
  return new Promise((resolve, reject) => {
    OnEventNotificationModel.find({ "on_events.name": eventType })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            resolve(result[0]._id);
          } else {
            let id = new mongoose.Types.ObjectId();
            let insertObj = {
              _id: id.toString(),
              title: "Comment",
              description: "",
              send_count: 1,
              status: 1,
              notifier_data: {
                title: "Comment",
                text_template_name: "event-OnCoverComment-text.tmpl",
                html_template_name: "",
                app_image_url: "",
                app_button_txt: "",
                img_url: "",
                click_url: "",
              },
              on_events: [
                {
                  name: eventType,
                },
              ],
            };
            OnEventNotificationModel.collection.insertOne(
              insertObj,
              function (err, response) {
                if (err) {
                  reject(err);
                } else {
                  resolve(response.insertedId.toString());
                }
              }
            );
          }
        }
      });
  });
};
const NotificationSave = (data) => {
  return new Promise((resolve, reject) => {
    let id = new mongoose.Types.ObjectId();
    let message = data.message;
    let insertObj = {
      _id: id.toString(),
      group_id: "",
      subscription_type: 1,
      subscription_ids: [],
      user_ids: [data.userId],
      status: 1,
      subject: "Comment",
      data: message,
      created_at: new Date(),
      updated_at: new Date(),
      send_by: 1,
      segments: [],
      initiated_by: 0,
      dashboard_user_id: "",
      on_event_notification_id: data.notificationEventId,
      seen: false,
      deep_link: data.deep_link,
      icon_url: data.icon_url,
    };
    NotificationModel.collection.insertOne(insertObj, function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve(response.insertedId.toString());
      }
    });
  });
};


User.search = (req) =>{
  let keyword = req.query.keyword;

  return new Promise((resolve, reject) => {
    let findcase = {
      $or: [
        { user_handle: {$regex: keyword}},
        { display_name: {$regex: keyword}},
        { first_name: {$regex: keyword} },
       // { email: {$regex: keyword}},
        
      ],
    };
    UserModel.find(findcase)
    .select({
      first_name: 1,
      last_name: 1,
      user_handle: 1,
      profile_img: 1,
    })
    .lean()
    .skip(req.query.page_no * req.query.limit)
    .limit(req.query.limit*1)
    .exec(function (err, response) {
      if (err) {
        reject(err);
      } else {
        if (response.length > 0) {
          resolve(response);
        } else {
          resolve([]);
        }
      }
    });
  });
}

User.list = (req) =>{
  return new Promise((resolve, reject) => {
    UserModel.find({})
    .select({
      _id : 1,
      login_type: 1,
      social_id: 1,
      email: 1,
      password: 1,
      first_name: 1,
      last_name: 1,
      artist_name: 1,
      contact_number: 1,
      profile_img: 1,
      extra: 1,
      status : 1,
      devices: 1,
      user_handle: 1,
      bio: 1,
      city: 1,
      display_name: 1,
      sex: 1,
      onboard_status: 1,
      singer_type: 1,
      isTeacher:1,
    })
    .lean()
    .exec(function (err, response) {
      if (err) {
        reject(err);
      } else {
        if (response.length > 0) {
          resolve(response)
        } else {
          resolve("No data found");
        }
      }
    });
  });
}

module.exports = User;
