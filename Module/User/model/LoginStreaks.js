"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");

// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.url);
const UserLoginStreaksSchema = require("../../../Schema/UserLoginStreaksSchema");
const UserAccountSchema = require("../../../Schema/UserAccountSchema");

let LoginStreaks = {};
const UserLoginStreaksModel = conUser.model(
  "USERLOGINSTREAKS",
  UserLoginStreaksSchema,
  "UserLoginStreaks"
);

const UserAccountModel = conUser.model(
  "USERACCOUNT",
  UserAccountSchema,
  "UserAccount"
);

LoginStreaks.createUserLoginStreaks = (req) => {
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  //check if user login today or not
  return new Promise((resolve, reject) => {
    let nowDate = new Date();
    var getDate = (nowDate.getDate() >= 1 && nowDate.getDate() <= 9) ? "0"+nowDate.getDate() : nowDate.getDate()
    var getMonth = (nowDate.getMonth() + 1);
    let date =
      nowDate.getFullYear() +
      "-" +
      ((getMonth >= 1 && getMonth <= 9) ? ("0"+getMonth) : getMonth) +
      "-" +
      getDate;
    UserLoginStreaksModel.find({ userId: userId, createdAt: date }).exec(
      function (err, result) {
        if (err) {
          throw err;
        } else {
          if (result.length > 0) {
            return resolve("Already logged in today");
          } else {
            let data = { userId: userId, createdAt: date };
            UserLoginStreaksModel.collection.insertOne(
              data,
              function (err, res) {
                if (err) {
                  reject(err);
                } else {
                  //finding streax
                  UserLoginStreaksModel.find({ userId: userId })
                    .sort({ createdAt: -1 })
                    .exec(function (err, result) {
                      (async function () {
                        let loginStreaksCount = await findUserConsecutiveLogin(
                          result
                        );
                        console.log("loginStreaksCount", loginStreaksCount);
                        saveXP(loginStreaksCount, userId);
                        resolve("Record inserted");
                      })();
                    });
                }
              }
            );
          }
        }
      }
    );
  });
};

let findUserConsecutiveLogin = (data) => {
  let loginStreaks = 0;
  for (var i = 0; i < data.length; i++) {
    var currentLoopDate = getYesterday(i);
    var getDate = (currentLoopDate.getDate() >= 1 && currentLoopDate.getDate() <= 9) ? "0"+currentLoopDate.getDate() : currentLoopDate.getDate()
    var getMonth = (currentLoopDate.getMonth() + 1);
    var todayLoopDate =
      currentLoopDate.getFullYear() +
      "-" +
      ((getMonth >= 1 && getMonth <= 9) ? ("0"+getMonth) : getMonth) +
      "-" +
      getDate;
    console.log("createdAt", data[i].createdAt);
    console.log("todayLoopDate", todayLoopDate);
    if (data[i].createdAt != todayLoopDate) {
      loginStreaks = 1;
      break;
    } else {
      loginStreaks = i+1;
    }
  }
  return loginStreaks;
};

let getUserConsecutiveLogin = (data) => {
  let loginStreaks = 0;
  for (var i = 0; i < data.length; i++) {
    var currentLoopDate = getYesterday(i);
    var getDate = (currentLoopDate.getDate() >= 1 && currentLoopDate.getDate() <= 9) ? "0"+currentLoopDate.getDate() : currentLoopDate.getDate()
    var getMonth = (currentLoopDate.getMonth() + 1);
    var todayLoopDate =
      currentLoopDate.getFullYear() +
      "-" +
      ((getMonth >= 1 && getMonth <= 9) ? ("0"+getMonth) : getMonth) +
      "-" +
      getDate;
    console.log("createdAt", data[i].createdAt);
    console.log("todayLoopDate", todayLoopDate);
    if (data[i].createdAt != todayLoopDate) {
     // loginStreaks = 1;
      break;
    } else {
      console.log('sssf')
      loginStreaks = i+1;
    }
  }
  return loginStreaks;
};

function getYesterday(i) {
  $today = new Date();
  $yesterday = new Date($today);
  $yesterday.setDate($today.getDate() - i);
  return $yesterday;
}

let saveXP = (loginStreaks, userId) => {
  
  let XP = 0;
  // for(var i=1;i<24;i++){
  //   for (var j = 1; j <= 10; j++) {

  //   }
  // }
  //for 1 to 10 days
  if (loginStreaks >= 1 && loginStreaks <= 9) {
    XP = 20;
  }
  if (loginStreaks == 10) {
    XP = 500;
  }
  //for 11 to 20 days
  if (loginStreaks >= 11 && loginStreaks <= 19) {
    XP = 40;
  }
  if (loginStreaks == 20) {
    XP = 1000;
  }
  //for 21 to 30 days
  if (loginStreaks >= 21 && loginStreaks <= 29) {
    XP = 60;
  }
  if (loginStreaks == 30) {
    XP = 2000;
  }
  //for 31 to 40 days
  if (loginStreaks >= 31 && loginStreaks <= 39) {
    XP = 80;
  }
  if (loginStreaks == 40) {
    XP = 4000;
  }
  //for 41 to 50 days
  if (loginStreaks >= 41 && loginStreaks <= 49) {
    XP = 100;
  }
  if (loginStreaks == 50) {
    XP = 8000;
  }
  //for 51 to 60 days
  if (loginStreaks >= 51 && loginStreaks <= 59) {
    XP = 120;
  }
  if (loginStreaks == 60) {
    XP = 16000;
  }
  //	err = session.DB(repo.database).C(UserAccountName).Update(bson.M{"_id": userId}, bson.M{"$inc": bson.M{"stats.xp": songxp}})
  UserAccountModel.findOne({ _id: userId }).exec(function (err, resp) {
    UserAccountModel.findOneAndUpdate(
      {
        _id: userId,
      },
      { 
        $set: {
          stats: {
            xp: (resp.stats.xp > 0) ?  resp.stats.xp + XP : XP,
            subscription: resp.stats.subscription,
            subscribe: resp.stats.subscribe,
            level: resp.stats.level,
            avg_time: resp.stats.avg_time,
            avg_tune: resp.stats.avg_tune,
            duration: resp.stats.duration,
            nextlevel: resp.stats.nextlevel,
            remaining_next_xp: resp.stats.remaining_next_xp,
            number_of_cover: resp.stats.number_of_cover,
            status: resp.stats.status,
            draftCount: resp.stats.draftCount,
            pendingxp: resp.stats.pendingxp
          }
      }
      },
      { new: true },
      function (err, res) {
        console.log("res", res);
      }
    );
  });
};

let calculateXP = (loginStreaks)=>{
  let XP = 0;
  //for 1 to 10 days
  if (loginStreaks >= 1 && loginStreaks <= 9) {
    XP = 20;
  }
  if (loginStreaks == 10) {
    XP = 500;
  }
  //for 11 to 20 days
  if (loginStreaks >= 11 && loginStreaks <= 19) {
    XP = 40;
  }
  if (loginStreaks == 20) {
    XP = 1000;
  }
  //for 21 to 30 days
  if (loginStreaks >= 21 && loginStreaks <= 29) {
    XP = 60;
  }
  if (loginStreaks == 30) {
    XP = 2000;
  }
  //for 31 to 40 days
  if (loginStreaks >= 31 && loginStreaks <= 39) {
    XP = 80;
  }
  if (loginStreaks == 40) {
    XP = 4000;
  }
  //for 41 to 50 days
  if (loginStreaks >= 41 && loginStreaks <= 49) {
    XP = 100;
  }
  if (loginStreaks == 50) {
    XP = 8000;
  }
  //for 51 to 60 days
  if (loginStreaks >= 51 && loginStreaks <= 59) {
    XP = 120;
  }
  if (loginStreaks == 60) {
    XP = 16000;
  }
  return XP
}

LoginStreaks.getUserLoginStreaks = (req) => {
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  return new Promise((resolve, reject) => {
    UserLoginStreaksModel.find({ userId: userId })
      .sort({ createdAt: -1 })
      .exec(function (err, result) {
        result.sort(function(a,b){
          // Turn your strings into dates, and then subtract them
          // to get a value that is either negative, positive, or zero.
          return new Date(b.createdAt) - new Date(a.createdAt);
        });
        // console.log("result=",result)
        (async function () {
          let loginStreaksCount = await getUserConsecutiveLogin(result);
          console.log("loginStreaksCount", loginStreaksCount);
          let data = {
            day : loginStreaksCount,
            xp : calculateXP(loginStreaksCount)
          };
          resolve(data);
          // db.UserAccount.updateOne({"_id":"6019524a579fb94428e81eea"},{$set:{"subscription.validity":"2023-01-01T00:00:00.000+00:00"}})
        })();
      });
  });
};

module.exports = LoginStreaks;
