var Concept = require('../model/Concept');
var Video = require("../model/Video");
exports.getLessonByConceptId = function(req, res) {
    Concept.getLessonByConceptId(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getTuneLessonByConceptId = function(req, res) {
    Concept.getTuneLessonByConceptId(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getTimingLessonByConceptId = function(req, res) {
    Concept.getTimingLessonByConceptId(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.UpdateVideoById = function(req, res) {
    Video.UpdateVideoById(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        console.log(err)
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};
