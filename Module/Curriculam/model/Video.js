const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");



mongoose.set("debug", true);

let conCurriculam = mongoose.createConnection(config.mongo.url);
const VideoSchema = require("../../../Schema/VideoSchema");
const VideoUploadHistorySchema = require("../../../Schema/VideoUploadHistorySchema");

const VideoModel = conCurriculam.model("VIDEO",VideoSchema,"Video");
const VideoUploadHistoryModel = conCurriculam.model("VIDEOUPLOADHISTORY",VideoUploadHistorySchema,"VideoUploadHistory");

let Video = {};

Video.UpdateVideoById = (req) => { //api for updateing public_url of video collection
    let videoId = req.query.VideoId;
    return new Promise((resolve, reject) => {
        VideoUploadHistoryModel.find({ videoId: videoId }).select({s3outPath : 1}).sort({_id:-1}).limit(1).exec(
            function(err,result){
                if(err){
                reject(err)
                }else{
                    if(result.length > 0){
                        let s3outPath = result[0].s3outPath;
                        let s3outPathArr = s3outPath.split("/");
                        let fileName = s3outPathArr[s3outPathArr.length -1];
                        let fileNameArr = fileName.split(".");
                        fileNameArr.pop();
                        let newFileName = fileNameArr.toString();
                        let filePath = "videoDir/Video/";
                        let fileNameWithPath = filePath+newFileName+".m3u8";
                        VideoModel.find({_id : videoId}).select({public_url :1 }).lean().exec(
                            function(err,result){
                                if(err){
                                reject(err)
                                }else{
                                    if(result.length > 0){
                                        let resultData = result[0];
                                        if(resultData.public_url != fileNameWithPath){
                                            VideoModel.findOneAndUpdate({_id : resultData._id}, {
                                                    $set: {
                                                        public_url: fileNameWithPath
                                                    }
                                                }, {new : true} , 
                                                function(err,data){
                                                    if(err){
                                                        reject(err)
                                                    }else{
                                                        resolve("updated")
                                                    }
                                                }
                                            );
                                            
                                        }else{
                                            resolve("Url Already present")
                                        }
                                    }
                                }
                            }
                        
                        )
                    }else{
                        resolve([])
                    }
                }
            }
        )
    })
    
}
module.exports = Video;