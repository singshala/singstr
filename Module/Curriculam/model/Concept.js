const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");



mongoose.set("debug", true);

let conCurriculam = mongoose.createConnection(config.mongo.url);
const LevelSchema = require("../../../Schema/LevelSchema");
const VideoSchema = require("../../../Schema/VideoSchema");
const GradeSchema = require("../../../Schema/GradeSchema");

let conUser = mongoose.createConnection(config.mongo.userUrl);
const UserAccountSchema = require("../../../Schema/UserAccountSchema");

let conTeacher = mongoose.createConnection(config.mongo.teacherUrl);
const TeacherSchema = require("../../../Schema/TeacherSchema");


const UserAccountModel = conUser.model(
    "USERACCOUNT",
    UserAccountSchema,
    "UserAccount"
  );
  

const LevelModel = conCurriculam.model(
    "LEVEL",
    LevelSchema,
    "Level"
  );

  const VideoModel = conCurriculam.model(
    "VIDEO",
    VideoSchema,
    "Video"
  );
  const GradeModel = conCurriculam.model(
    "GRADE",
    GradeSchema,
    "Grade"
  );

const TeacherModel = conTeacher.model(
    "TEACHER",
    TeacherSchema,
    "teacher"
  );

exports.getLessonByConceptId = (req) => {
    let difficultyArr = ["NotSpecified", "Beginner", "Intermediate", "Advanced"];
    let conceptId = req.params.conceptId;
    return new Promise((resolve, reject) => {
        GradeModel.find({_id: conceptId}).select({display_name : 1,name:1,order : 1}).lean().exec(
        function (err, conceptResult) {
            if (err) {
                throw err;
            }else{
                if(conceptResult.length > 0){
                    LevelModel.find({"grade_id" : conceptId}).select({popular_songs :0 ,progression_steps : 0, course_required_songs : 0}).lean().exec(
                        function (err, result) {
                            if (err) {
                            throw err;
                            }else{
                                if(err){
                                    reject(err)
                                }else{
                                    if(result.length > 0){

                                        (async function() {
                                            let allTeacher =  await getTeacher();
                                            let allVideo = await getVideo();

                                            const resultData = result.map((item) => {
                                                let teacherDetails = allTeacher.find((teacherObj) => teacherObj._id == item.teacher_id)
                                                let teacher = ( teacherDetails != undefined && teacherDetails != null && Object.keys(teacherDetails).length > 0 ) ? teacherDetails : "";
                                                delete item.teacher_id;

                                                let videoDetails = allVideo.find((videoObj) => videoObj._id == item.video_id)
                                                let video = (videoDetails != undefined && videoDetails != null && Object.keys(videoDetails).length > 0) ? videoDetails : "";
                                                delete item.video_id;

                                                item.difficulty = difficultyArr[item.difficulty];

                                                if(item.subscription_purchase_type == 1){
                                                    item.subscription_purchase_type = "Free"
                                                }else if(item.subscription_purchase_type == 3) {
                                                    item.subscription_purchase_type = "Paid"
                                                }else{
                                                    item.subscription_purchase_type = ""
                                                }
                                                
                                                item = {...item,teacher,video};
                                                return item
                                            })

                                            let finalResult = conceptResult.map((concept) => {
                                                let lessonsData =  resultData ;
                                                return concept = {...concept,lessonsData}        
                                            })
                                        
                                            resolve(finalResult);
                                        })()
                                        
                                    }else{
                                        resolve("not found")
                                    }
                                }
                            }
                        }
                    );
                }else{
                    resolve("not found")
                }
            }
        });
    });
}

exports.getTuneLessonByConceptId = (req) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    let difficultyArr = ["NotSpecified", "Beginner", "Intermediate", "Advanced"];
    let conceptId = req.params.conceptId;
    return new Promise((resolve, reject) => {
        UserAccountModel.find({_id: userId}).lean().exec(
        function (err, userResult) {
            if (err) {
                throw err;
            }else{
                if(userResult.length > 0){
                    LevelModel.find({"grade_id" : conceptId, primary_tag:"Tune"}).select({popular_songs :0 ,progression_steps : 0, course_required_songs : 0}).lean().exec(
                        function (err, result) {
                            if (err) {
                            throw err;
                            }else{
                                if(err){
                                    reject(err)
                                }else{
                                    if(result.length > 0){

                                        (async function() {
                                            let allVideo = await getVideo();

                                            const resultData = result.map((item) => {
                                                
                                                let videoDetails = allVideo.find((videoObj) => videoObj._id == item.video_id)
                                                let video = (videoDetails != undefined && videoDetails != null && Object.keys(videoDetails).length > 0) ? videoDetails : "";
                                                delete item.video_id;

                                                item.difficulty = difficultyArr[item.difficulty];

                                                if(item.subscription_purchase_type == 1){
                                                    item.subscription_purchase_type = "Free"
                                                }else if(item.subscription_purchase_type == 3) {
                                                    item.subscription_purchase_type = "Paid"
                                                }else{
                                                    item.subscription_purchase_type = ""
                                                }
                                                
                                                item = {...item,video};
                                                return item
                                            })

                                            // let finalResult = conceptResult.map((concept) => {
                                            //     let lessonsData =  resultData ;
                                            //     return concept = {...concept,lessonsData}        
                                            // })
                                            let finalResult = []
                                            let timingObj = {
                                                title : "Improve your Tune",
                                                score : userResult[0].stats.xp,
                                                lessonsData : resultData
                                            }
                                            finalResult.push(timingObj)
                                        
                                            resolve(finalResult);
                                        })()
                                        
                                    }else{
                                        resolve("not found")
                                    }
                                }
                            }
                        }
                    );
                }else{
                    resolve("not found")
                }
            }
        });
    });
}

exports.getTimingLessonByConceptId = (req) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    let difficultyArr = ["NotSpecified", "Beginner", "Intermediate", "Advanced"];
    let conceptId = req.params.conceptId;
    return new Promise((resolve, reject) => {
        UserAccountModel.find({_id: userId}).lean().exec(
            function (err, userResult) {
                if (err) {
                    throw err;
                }else{
                    if(userResult.length > 0){
                        LevelModel.find({"grade_id" : conceptId, primary_tag:"Timing"}).select({popular_songs :0 ,progression_steps : 0, course_required_songs : 0}).lean().exec(
                                function (err, result) {
                                    if (err) {
                                    throw err;
                                    }else{
                                        if(err){
                                            reject(err)
                                        }else{
                                            if(result.length > 0){

                                                (async function() {
                                                    let allVideo = await getVideo();

                                                    const resultData = result.map((item) => {
                                                        
                                                        let videoDetails = allVideo.find((videoObj) => videoObj._id == item.video_id)
                                                        let video = (videoDetails != undefined && videoDetails != null && Object.keys(videoDetails).length > 0) ? videoDetails : "";
                                                        delete item.video_id;

                                                        item.difficulty = difficultyArr[item.difficulty];

                                                        if(item.subscription_purchase_type == 1){
                                                            item.subscription_purchase_type = "Free"
                                                        }else if(item.subscription_purchase_type == 3) {
                                                            item.subscription_purchase_type = "Paid"
                                                        }else{
                                                            item.subscription_purchase_type = ""
                                                        }
                                                        
                                                        item = {...item,video};
                                                        return item
                                                    })
                                                    let finalResult = []
                                                    let timingObj = {
                                                        title : "Improve Your Timing",
                                                        score :  userResult[0].stats.xp,
                                                        lessonsData : resultData
                                                    }
                                                    finalResult.push(timingObj)
                                                
                                                    resolve(finalResult);
                                                })()
                                                
                                            }else{
                                                resolve([])
                                            }
                                        }
                                    }
                                }
                            );
                    }else{
                        resolve([])
                    }
                }
            }
        )
                
    })        
        
    
}


const getTeacher = () => {
    return new Promise((resolve, reject) => {
        const URL = "https://s3.ap-south-1.amazonaws.com/stage.singstr.content/";
        TeacherModel.find().select({name:1,bio:1,profileImgUrl:1,experience:1}).exec(
            function (err, result) {
                if (err) {
                  throw err;
                }else{
                    if(err){
                        reject(err)
                    }else{
                        //return this._id.toHexString();
                        const finalData = result.map((obj) => {
                           obj.profileImgUrl = URL + obj.profileImgUrl;
                            return obj;
                        })
                        resolve(finalData)
                    }
               }
            }
        );
    });
}

const getVideo = () => {
    return new Promise((resolve, reject) => {
        const URL = "https://d286snw20yvi04.cloudfront.net/";
        VideoModel.find().lean().exec(
            function (err, result) {
                if (err) {
                  throw err;
                }else{
                    if(err){
                        reject(err)
                    }else{
                        //return this._id.toHexString();
                        const finalData = result.map((obj) => {
                            obj.video_url = ( obj.video_url != "") ? URL+obj.video_url : "";
                            obj.thumbnail_url = URL + obj.thumbnail_url;
                            return obj;
                        })
                        resolve(finalData)
                    }
               }
            }
        );
    });
}  



