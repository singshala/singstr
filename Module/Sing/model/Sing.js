'user strict';
const mongoose = require("mongoose");
const config = require('../config.json');
 
 

// mongoose.Promises = global.Promises;
mongoose.set('debug', true)
let conSing = mongoose.createConnection(config.mongo.url);
const SingSchema = require("../../../Schema/SingSchema");


let Sing = {}
const SingModel = conSing.model('SING',SingSchema,'Sing');
Sing.getTrendingSong  = (req) => { 
    console.log("req=======",req.query.genre)
    let difficulty = req.query.difficulty
    let genre = req.query.genre
    var perPage = 10, page = Math.max(0, req.query.page)
    //if difficulty and genre are undefined : 

    if(difficulty==undefined && genre==undefined) {
        return new Promise((resolve, reject) => {
            SingModel.find(
                {
                  status: 1,
                  coverCount : {"$gte": 1}
                }
            ).sort("-coverCount").limit(perPage)
            .skip(perPage * page).exec(async function (err, result) {
                console.log("ERR========",err)
                // console.log("result========",result)
                if (err) {
                  reject(err);
                } else {
                    (async function() {
                        let filteredData =  await filterThumbNail(result)
                        resolve(filteredData);
                      })()
                }
            });
        })
    }  
    
    if(difficulty!=undefined && genre!=undefined) {
        // session.DB(repo.database).C(singRepoName).Find(bson.M{"status": status.Avail, "coverCount": bson.M{"$gte": 1}, "difficulty": bson.M{"$regex": difficulty, "$options": "i"}, "genres": bson.M{"$elemMatch": bson.M{"$regex": genre, "$options": "i"}}}).Sort("-coverCount").Limit(10).All(&sings)
        return new Promise((resolve, reject) => {
            SingModel.find(
                {
                  status: 1,
                  coverCount : {"$gte": 1},
                  difficulty: {"$regex": difficulty, "$options": "i"},
                  genres: {"$elemMatch": {"$regex": genre, "$options": "i"}}
                }
            ).sort("-coverCount").limit(perPage)
            .skip(perPage * page).exec(async function (err, result) {
                console.log("ERR========",err)
                // console.log("result========",result)
                if (err) {
                  reject(err);
                } else {
                    (async function() {
                        let filteredData =  await filterThumbNail(result)
                        resolve(filteredData);
                      })()
                }
            });
        })
    }  

    if(difficulty!=undefined && genre==undefined) {
        return new Promise((resolve, reject) => {
            SingModel.find(
                {
                  status: 1,
                  coverCount : {"$gte": 1},
                  difficulty: {"$regex": difficulty, "$options": "i"},
                }
            ).sort("-coverCount").limit(perPage)
            .skip(perPage * page).exec(async function (err, result) {
                console.log("ERR========",err)
                // console.log("result========",result)
                if (err) {
                  reject(err);
                } else {
                    (async function() {
                        let filteredData =  await filterThumbNail(result)
                        resolve(filteredData);
                      })()
                }
            });
        })

    }
    
    if(difficulty==undefined && genre!=undefined) {
        return new Promise((resolve, reject) => {
            SingModel.find(
                {
                  status: 1,
                  coverCount : {"$gte": 1},
                  genres: {"$elemMatch": {"$regex": genre, "$options": "i"}}
                }
            ).sort("-coverCount").limit(10).exec(async function (err, result) {
                console.log("ERR========",err)
                // console.log("result========",result)
                if (err) {
                  reject(err);
                } else {
                    
                  (async function() {
                    let filteredData =  await filterThumbNail(result)
                    resolve(filteredData);
                  })()
                }
            });
        })

    }

          
}  

Sing.getSongURL = (req) => {
  const songId = req.params.songId;
  return new Promise((resolve,reject) => {
    SingModel.find({_id:songId}).select({preview_url : 1,media_url :1}).exec(
      function(err,result){
        if(err){
          reject(err)
        }else{
          if(result.length > 0){
            (async function() {
              let filteredData =  await filterURL(result)
              resolve(filteredData);
            })()
          }else{
            resolve("song not found")
          }
        }
      }
    )
  })
}

let filterThumbNail = (req)=>{
    for(let i=0;i<req.length;i++){
        req[i].thumbnail_url = "http://d2zkzn6c850vk.cloudfront.net/thumbnail/"+req[i].thumbnail_url
    }
    return req
}

let filterURL = (req) => {
  for(let i = 0 ; i < req.length; i++){
    req[i].preview_url = (req[i].preview_url) ? "http://d2zkzn6c850vk.cloudfront.net/"+req[i].preview_url : "";
    req[i].media_url = (req[i].media_url) ? "http://d2zkzn6c850vk.cloudfront.net/"+req[i].media_url : "";
  }
  return req;
}


module.exports= Sing;
