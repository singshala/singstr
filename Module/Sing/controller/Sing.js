var Sing = require('../model/Sing');
exports.getTrendingSong = function(req, res) {
    Sing.getTrendingSong(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getSongURL = function(req,res)  {
    Sing.getSongURL(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};