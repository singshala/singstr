var Contest = require('../model/Contest');
exports.listAllItem = function(req, res) {
    Contest.listAllItem(req)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.listItemById = function(req, res) {
    Contest.listItemById(req.params.contestId)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.createContest = function(req, res) {
    Contest.createContest(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.deleteItemById = function(req, res) {
    Contest.deleteItemById(req.params.contestId)
    .then((result) => {
        let response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.createContestEntry = function(req, res) {
    Contest.createContestEntry(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.markAttemptAsContest = function(req, res) {
    Contest.markAttemptAsContest(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.entryAttemptList = function(req, res) {
    Contest.entryAttemptList(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.contestVote = function(req, res) {
    Contest.contestVote(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.markWinner = function(req, res) {
    Contest.markWinner(req,res)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.markInvalid = function(req, res) {
    Contest.markInvalid(req.params.id)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.getPutObjectUrl = function(req, res) {
    Contest.getPutObjectUrl(req)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.deleteById = function(req, res) {
    Contest.deleteById(req)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.entryDelete = function(req, res) {
    console.log("123")
    Contest.entryDelete(req)
    .then((result) => {
        let response = {
            status: 201,
            message: "SUCCESS",
            data: result,
        }
        res.status(201).send(response);
    }).catch((err) => {
        let response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};
