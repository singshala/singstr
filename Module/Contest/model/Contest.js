"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");

const AWS = require('aws-sdk');

mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conContest = mongoose.createConnection(config.mongo.url);
let conWatch = mongoose.createConnection(config.mongo.watchUrl);
let conUser = mongoose.createConnection(config.mongo.userUrl);
const UserSchema = require("../../../Schema/UserSchema");
const ContestSchema = require("../../../Schema/ContestSchema");
const ContestEntrySchema = require("../../../Schema/ContestEntrySchema");
const AttemptSchema = require("../../../Schema/AttemptSchema");

let conSing = mongoose.createConnection(config.mongo.singUrl);
const SingSchema = require("../../../Schema/SingSchema");
const { isArray } = require("util");

let Contest = {};
const ContestModel = conContest.model("CONTEST", ContestSchema, "Contest");
const SingModel = conSing.model("SING", SingSchema, "Sing");
const ContestEntryModel = conContest.model(
  "CONTEST_ENTRY",
  ContestEntrySchema,
  "Contest_Entry"
);
const attemptModel = conWatch.model("ATTEMPT", AttemptSchema, "Attempt");
const UserModel = conUser.model("USER", UserSchema, "User");
var credentials = new AWS.SharedIniFileCredentials({profile: 'singshala_node'});
AWS.config.credentials = credentials;
AWS.config.update({region: 'ap-south-1'});
let s3 = new AWS.S3({
  signatureVersion: 'v4'
});


Contest.listItemById = (itemId) => {
  return new Promise((resolve, reject) => {
    ContestModel.findOne({
      _id: itemId,
    }).exec(async function (err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
        mongoose.connection.close();
      }
    });
  });
};

Contest.listAllItem = () => {
  return new Promise((resolve, reject) => {
    ContestModel.find({}).exec(async function (err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
        mongoose.connection.close();
      }
    });
  });
};

Contest.createContest = async (req, res) => {
  let contestData = req.body;
  if (!contestData.ContestName) {
    return res
      .status(400)
      .send({ status: 400, message: "ContestName is required!!" });
  }
  if (!contestData.StartDate) {
    return res
      .status(400)
      .send({ status: 400, message: "StartDate is required!!" });
  }
  if (!contestData.SubmissionEndDate) {
    return res
      .status(400)
      .send({ status: 400, message: "SubmissionEndDate is required!!" });
  }
  if (!contestData.WinnerAnnouncementDate) {
    return res
      .status(400)
      .send({ status: 400, message: "WinnerAnnouncementDate is required!!" });
  }
  if (!contestData.DashboardBannerContents) {
    return res
      .status(400)
      .send({ status: 400, message: "DashboardBannerContents is required!!" });
  }
  if (!contestData.EligibilityLevel) {
    return res
      .status(400)
      .send({ status: 400, message: "EligibilityLevel is required!!" });
  }
  if (!contestData.HostName) {
    return res
      .status(400)
      .send({ status: 400, message: "HostName is required!!" });
  }
  if (!req.body.HostLogo) {
    return res
      .status(400)
      .send({ status: 400, message: "HostLogo is required!!" });
  }
  if (!contestData.ContestType) {
    return res
      .status(400)
      .send({ status: 400, message: "ContestType is required!!" });
  }
  if (!req.body.ContestVideo) {
    return res
      .status(400)
      .send({ status: 400, message: "ContestVideo is required!!" });
  }
  if (!contestData.Prize) {
    return res
      .status(400)
      .send({ status: 400, message: "Prize is required!!" });
  }
  if (!contestData.HowToEnter) {
    return res
      .status(400)
      .send({ status: 400, message: "HowToEnter is required!!" });
  }
  if (!contestData.Rules) {
    return res
      .status(400)
      .send({ status: 400, message: "Rules is required!!" });
  }
  if (!contestData.JudgingCriteria) {
    return res
      .status(400)
      .send({ status: 400, message: "JudgingCriteria is required!!" });
  }
  if (!contestData.SongList) {
    return res
      .status(400)
      .send({ status: 400, message: "SongList is required!!" });
  }
  if (!isArray(contestData.SongList)) {
    return res
      .status(400)
      .send({ status: 400, message: "SongList must be array type!!" });
  }

  if (contestData.SongList.length == 0) {
    return res
      .status(400)
      .send({ status: 400, message: "SongList cannot be an empty array!!" });
  }
  let id = new mongoose.Types.ObjectId();

  let data = {
    _id: id.toString(),
    ContestName: contestData.ContestName,
    StartDate: contestData.StartDate,
    SubmissionEndDate: contestData.SubmissionEndDate,
    WinnerAnnouncementDate: contestData.WinnerAnnouncementDate,
    DashboardBannerContents: contestData.DashboardBannerContents,
    EligibilityLevel: contestData.EligibilityLevel,
    HostName: contestData.HostName,
    HostLogo: req.body.HostLogo,
    ContestType: contestData.ContestType,
    ContestVideo: req.body.ContestVideo,
    Prize: contestData.Prize,
    HowToEnter: contestData.HowToEnter,
    Rules: contestData.Rules,
    JudgingCriteria: contestData.JudgingCriteria,
    SongList: contestData.SongList,
  };

  //   let songList = req.body.SongList;
  //   return new Promise((resolve, reject) => {
  //     checkSongById(songList).then((value)=>{
  //         console.log("value",value)
  //         resolve(value)
  //     });
  //   })
  return new Promise((resolve, reject) => {
    // (async function () {
    //   let isValidSong = checkSongById(songList);
    //   console.log("isValidSong",isValidSong)
    //   if (isValidSong) {
    //     ContestModel.collection.insertOne(data, function (err, response) {
    //       if (err) {
    //         reject(err);
    //       } else {
    //         resolve("Record inserted");
    //       }
    //     });
    //   } else {
    //     reject("Invalid songId!!");
    //   }
    // })();
    ContestModel.collection.insertOne(data, function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve("Record inserted");
      }
    });
  });
};

Contest.deleteItemById = (itemId) => {
  return new Promise((resolve, reject) => {
    ContestModel.findOneAndRemove({ _id: itemId }, function (err, result) {
      if (err) {
        reject(err);
      } else {
        resolve(result);
        mongoose.connection.close();
      }
    });
  });
};

let checkSongById = async (songList) => {
  let results = [];
  for (let i = 0; i < songList.length; i++) {
    await SingModel.find({
      _id: songList[i],
    }).exec(async function (err, result) {
      if (result.length > 0) {
        results.push(result);
      }
      if (i == songList.length - 1) {
        console.log("sadas");
        return results;
      }
    });
  }
};

let findSong = (songId) => {
  SingModel.find({
    _id: songId,
  }).exec(async function (err, result) {
    if (err) {
      return err;
    } else {
      return result;
    }
  });
};

Contest.createContestEntry = async (req, res) => {
  let contestData = req.body;
  if (!contestData.contestId) {
    return res
      .status(400)
      .send({ status: 400, message: "contestId is required!!" });
  }
  if (!contestData.attemptId) {
    return res
      .status(400)
      .send({ status: 400, message: "attemptId is required!!" });
  }
  if (!contestData.userId) {
    return res
      .status(400)
      .send({ status: 400, message: "userId is required!!" });
  }
  let id = new mongoose.Types.ObjectId();
  let data = {
    _id: id.toString(),
    contestId: contestData.contestId,
    attemptId: contestData.attemptId,
    userId: contestData.userId,
    date: new Date().toString(),
    votes: [],
    isWinner: false,
    isValid: true,
  };

  return new Promise((resolve, reject) => {
    attemptModel.findOne(
      { _id: contestData.attemptId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("attemptId not found");
          }
        }
      }
    );
    UserModel.findOne({ _id: contestData.userId }, function (err, response) {
      if (err) {
        reject(err);
      } else {
        if (response == null) {
          reject("userId not found");
        }
      }
    });
    ContestModel.findOne(
      { _id: contestData.contestId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("contestId not found");
          }
        }
      }
    );
    ContestEntryModel.find(
      {
        contestId: contestData.contestId,
        userId: contestData.userId,
        attemptId: contestData.attemptId,
      },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            reject("Entry is already made for this contest");
          } else {
            ContestEntryModel.collection.insertOne(
              data,
              function (err, response) {
                if (err) {
                  reject(err);
                } else {
                  resolve("Record inserted");
                  attemptModel.findOneAndUpdate(
                    { _id: contestData.attemptId },
                    {
                      $set: {
                        contestId: contestData.contestId,
                        isContest: true
                      },
                    },
                    { new: true },
                    function (err, data) {
                      if (err) {
                        reject(err);
                      } else {
                        resolve("Attempt marked as contest");
                      }
                    }
                  );
                }
              }
            );
          }
        }
      }
    );
  });
};


Contest.entryAttemptList = async (req, res) => {
  let contestData = req.body;
  if (!contestData.contestId) {
    return res
      .status(400)
      .send({ status: 400, message: "contestId is required!!" });
  }

  return new Promise((resolve, reject) => {
    ContestModel.findOne(
      { _id: contestData.contestId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("contestId not found");
          }
        }
      }
    );
    ContestEntryModel.find(
      { contestId: contestData.contestId, isValid: true },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          (async function(){

          for(let i=0;i<response.length;i++){
            let attempt = await fetchAttemptData(response[i].attemptId);
            let cover_url = (attempt.media.cover_url != "") ? attempt.media.cover_url : "";
            cover_url = String(cover_url);
            let coverArr = (cover_url != "") ? cover_url.split("/") : [];
            let cover_img = (coverArr.length > 0) ? coverArr[coverArr.length-1] : "";

            attempt.media.cover_url = "https://d286snw20yvi04.cloudfront.net/"+cover_img;
            
            let public_media_url = (attempt.media.public_media_url != "") ? attempt.media.public_media_url : "";
            public_media_url = String(public_media_url);

            let publicMediaArr = (public_media_url != "") ? public_media_url.split("/") : [];
            let public_media = (publicMediaArr.length > 0) ? publicMediaArr[publicMediaArr.length-1] : "";
            attempt.media.public_media_url= "https://d286snw20yvi04.cloudfront.net/"+public_media;
            //attempt.user = 1;

            let userDetails = await fetchUserData(response[i].userId)
            attempt.user = userDetails;
           // console.log("h2", response[i].user)

            let songDetails = await fetchSongData(attempt.reference_id)
            attempt.song = songDetails;
           // console.log("h3", response[i].sing)
            
            response[i].attempt = attempt;
          //  console.log("h1", response[i].attempt)
            
          
            
           }
           resolve(response);
        })()
        }
      }
    );
  });
};

let fetchAttemptData = (attemptId)=>{
  return new Promise((resolve, reject) => {
  attemptModel.findOne(
    { _id: attemptId },
    function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve(response._doc);
      }
    }
  );
  })
}

let fetchUserData = (userId)=>{
  return new Promise((resolve, reject) => {
  UserModel.find(
    { _id: userId },
    function (err, response) {
      if (err) {
        reject(err);
      } else {
        let responseNew = {};
        responseNew ={
          userId : response[0]._id,
          profile_img : response[0].profile_img,
          display_name : response[0].display_name,
          user_handle : response[0].user_handle,
        }
        resolve(responseNew);
      }
    }
  );
  })
}

let fetchSongData = (reference_id)=>{
  return new Promise((resolve, reject) => {
  SingModel.find(
    { _id: reference_id },
    function (err, response) {
      if (err) {
        reject(err);
      } else {
        let responseNew = {}
        responseNew = {
          _id: response[0]._id,
          title: response[0].title,
          album: response[0].album,
          year: response[0].year,
          lyrics_start_time: response[0].lyrics_start_time,
          lyrics: response[0].lyrics,
          thumbnail_url: response[0].thumbnail_url,
          difficulty: response[0].difficulty,
          artists: response[0].artists,

   }
        resolve(responseNew);
      }
    }
  );
  })
}

Contest.contestVote = async (req, res) => {
  let contestData = req.body;
  if (!contestData.contestId) {
    return res
      .status(400)
      .send({ status: 400, message: "contestId is required!!" });
  }
  if (!contestData.attemptId) {
    return res
      .status(400)
      .send({ status: 400, message: "attemptId is required!!" });
  }

  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;

  return new Promise((resolve, reject) => {
    attemptModel.findOne(
      { _id: contestData.attemptId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("attemptId not found");
          }
        }
      }
    );
    ContestModel.findOne(
      { _id: contestData.contestId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("contestId not found");
          }
        }
      }
    );
    ContestEntryModel.find(
      { contestId: contestData.contestId, attemptId: contestData.attemptId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            let votes = [];
            votes = response[0].votes;
            votes.push({ userId: userId });
            ContestEntryModel.find(
              { votes: { $elemMatch: { userId: userId } } },
              function (err, response) {
                if (err) {
                  reject(err);
                } else {
                  if (response.length > 0) {
                    reject("You have already voted");
                  } else {
                    ContestEntryModel.findOneAndUpdate(
                      {
                        contestId: contestData.contestId,
                        attemptId: contestData.attemptId,
                      },
                      {
                        $set: {
                          votes: votes                          
                        },
                      },
                      { new: true },
                      function (err, data) {
                        if (err) {
                          reject(err);
                        } else {
                          resolve("voting is successfully done");
                        }
                      }
                    );
                  }
                }
              }
            );
          } else {
            reject("contest entry not found");
          }
        }
      }
    );
  });
};

Contest.markWinner = async (req, res) => {
  let contestData = req.body;
  if (!contestData.contestId) {
    return res
      .status(400)
      .send({ status: 400, message: "contestId is required!!" });
  }
  if (!contestData.attemptId) {
    return res
      .status(400)
      .send({ status: 400, message: "attemptId is required!!" });
  }


  return new Promise((resolve, reject) => {
    attemptModel.findOne(
      { _id: contestData.attemptId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("attemptId not found");
          }
        }
      }
    );
    ContestModel.findOne(
      { _id: contestData.contestId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response == null) {
            reject("contestId not found");
          }
        }
      }
    );
    ContestEntryModel.find(
      { contestId: contestData.contestId, attemptId: contestData.attemptId },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            ContestModel.findOneAndUpdate(
              {
                _id: contestData.contestId,
              },
              {
                $set: {
                  Winner: {
                    attemptId: contestData.attemptId
                  }
                },
              },
              { new: true },
              function (err, data) {
                if (err) {
                  reject(err);
                } else {
                  ContestEntryModel.findOneAndUpdate(
                    { contestId: contestData.contestId, attemptId: contestData.attemptId },
                    {
                      $set: {
                        isWinner: true
                      },
                    },
                    { new: true },
                    function (err, data) {
                      if (err) {
                        reject(err);
                      } else {
                        resolve("winner is successfully added");
                      }
                    }
                  );
                  resolve("winner is successfully added");
                }
              }
            );
          } else {
            reject("contest entry not found");
          }
        }
      }
    );
  });
};

Contest.markInvalid = async (id) => {

  return new Promise((resolve, reject) => {
    ContestEntryModel.find(
      { _id: id },
      function (err, response) {
        if (err) {
          reject(err);
        } else {
          if (response.length > 0) {
            ContestEntryModel.findOneAndUpdate(
              {
                _id: id,
              },
              {
                $set: {
                  isValid: false
                },
              },
              { new: true },
              function (err, data) {
                if (err) {
                  reject(err);
                } else {
                  resolve("contest entry is marked invalid");
                }
              }
            );
          } else {
            reject("contest entry not found");
          }
        }
      }
    );
  });
};


Contest.getPutObjectUrl = (req) =>{
  return new Promise((resolve, reject) =>{
   try{
  if (!req.query.imageType) {
    reject("Please enter type of image");
  } 
  if (!req.query.type) {
    reject("Please enter type of media");
  }
  let _id = new mongoose.Types.ObjectId();
  
   let url1= _id.toString();
    let imageType = "general";
   if (req.query.imageType=="contest") {
    imageType = "contest";
   } else if (req.query.imageType=="teacher") {
    imageType = "teacher/thumbnail";
  }

    let params = {
        Bucket: 'singstr.content',
        Key: "public/"+imageType+"/"+url1+"."+req.query.type,
        Expires: 60 * 5,
        ContentType:'*/*',
      };
      s3.getSignedUrl('putObject', params, function (err, url) {
        console.log("url="+url)
        resolve(url)
      });

    } catch (e) {
      console.log(e.code);
      console.log(e.msg);
      reject(e);
    }
   })

};

Contest.deleteById = (req) => {
  let contestId = req.query.contestId;
  return new Promise((resolve, reject) => {
    ContestModel.find({ _id: contestId }, function (err, response) {
      if(err){
        reject(err)
      }else{
        if(response.length >0){
          ContestModel.findOneAndDelete({ _id: contestId }, function (err, response) {
            if (err) {
              reject(err);
            } else {
              resolve("contest deleted");
              
            }
          })
        }else{
          reject("contestId not found")
        }
      }
    })
  });
};

Contest.entryDelete = (req) => {
  let attempt = req.params.attemptId;
  console.log(attempt)
  return new Promise((resolve, reject) => {
    ContestEntryModel.find({attemptId: attempt}, function(err, response) {
      if(err){
        reject(err);
      }else{
        if(response.length >0){
          ContestEntryModel.findOneAndDelete({ attemptId: attempt }, function (err, response) {
            if (err) {
              reject(err);
            } else {
              resolve("contest deleted");
            }
          })
        }else{
          reject("Invalid input")
        }
      }
    })
  })
}


module.exports = Contest;
