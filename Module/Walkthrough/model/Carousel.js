"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");


// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conWalkthrough = mongoose.createConnection(config.mongo.url);

const CarouselSchema = require("../../../Schema/CarouselSchema");

let Carousel = {};
const CarouselModel = conWalkthrough.model("CAROUSEL", CarouselSchema, "Carousel");

Carousel.deleteCarousel = (req) => {
    let carouselId = req.query.id;
    console.log(carouselId)
    return new Promise((resolve, reject) => {
        CarouselModel.findOneAndDelete({ _id: carouselId }).exec(
            function(err,result){
                if(err){
                reject(err)
                }else{
                    resolve("deleted")
                }
            }
        )
    })
}
module.exports = Carousel