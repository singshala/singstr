let Feed = require("../model/Feed");
let Cover = require("../model/Cover");
let Comment = require("../model/Comment");

exports.getFollowingFeed = function (req, res) {
  Feed.getFollowingFeed(req)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
};

exports.getCoverSubmitedByUser = function (req, res) {
  Cover.getCoverSubmitedByUser(req)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
};

exports.AddReply = function (req, res) {
  Comment.AddReply(req)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
};

exports.GetCommentById = function (req, res) {
  Comment.GetCommentById(req)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
};

exports.GetDraft = function (req, res) {
  Cover.getCoverDraft(req, res)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        count: result.length,
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
};

exports.downloadVideo = function (req, res) {
  (async function () {
 await Cover.downloadVideo(req, res)
    .then((result) => {
      let response = {};
      response = {
        status: 200,
        message: "SUCCESS",
        data: result,
      };
      res.status(200).send(response);
    })
    .catch((err) => {
      let response = {};
      response = {
        status: 404,
        message: "ERROR",
        error: err,
      };
      res.status(404).send(response);
    });
  })();
};


exports.AddComment = function (req, res) {
    Comment.AddComment(req)
      .then((result) => {
        let response = {};
        response = {
          status: 200,
          message: "SUCCESS",
          data: result,
        };
        res.status(200).send(response);
      })
      .catch((err) => {
        let response = {};
        response = {
          status: 404,
          message: "ERROR",
          error: err,
        };
        res.status(404).send(response);
      });
  };

  exports.getS3Object = function(req, res) {
    Cover.getS3Object(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.updateCoverMediaUrlStatusForStaging = function(req, res) {
  Cover.updateCoverMediaUrlStatusForStaging(req)
  .then((result) => {
      let response = {}
      response = {
          status: 200,
          message: "SUCCESS",
          data: result,
      }
      res.status(200).send(response);
  }).catch((err) => {
      let response = {}
      response = {
          status: 404,
          message: "ERROR",
          error: err,
      }
      res.status(404).send(response);
  })
};
