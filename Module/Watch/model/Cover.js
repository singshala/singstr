"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");
var ffmpeg = require('ffmpeg');
const AWS = require('aws-sdk');
const fs = require("fs");
const client = require('https');
var credentials = new AWS.SharedIniFileCredentials({profile: 'singshala_node'});
AWS.config.credentials = credentials;
let s3 = new AWS.S3({
  signatureVersion: 'v4'
});

// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conWatch = mongoose.createConnection(config.mongo.watchUrl);
let conSing = mongoose.createConnection(config.mongo.singUrl);
let conUser = mongoose.createConnection(config.mongo.userUrl);

const attemptSchema = require("../../../Schema/AttemptSchema");
const attempUploadSchema = require("../../../Schema/AttemptUploadSchema");
const SingSchema = require("../../../Schema/SingSchema");
const UserSchema = require("../../../Schema/UserSchema");

let Cover = {};
const attemptModel = conWatch.model("ATTEMPT",attemptSchema,"Attempt");
const AttemptUploadModel = conWatch.model("ATTEMPTUPLOAD",attempUploadSchema,"AttemptUpload");
const SingModel = conSing.model("SING",SingSchema,"Sing");
const UserModel = conUser.model("USER", UserSchema, "User");



Cover.getCoverSubmitedByUser = (req) => {
    let Url = "https://d286snw20yvi04.cloudfront.net/";
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    let reqUserId = (req.query.other_user_id) ?  req.query.other_user_id : userId
    return new Promise((resolve, reject) => {
        attemptModel.find({user_id : reqUserId,status : 3,'media.cover_url' : {$ne : ""}}).sort({time:-1}).lean().exec(
            function(err,result){
                if(err){
                    reject(err)
                }else{
                    if(result.length > 0){
                        (async function() {
                            let submitArr = [];
                            let AllAttemptSongsIds = await AttemptUploadModel.find({user_id:reqUserId}).select({song_id:1,attempt_id:1});
                            let AllSongsIds = AllAttemptSongsIds.map((song) => song.song_id)
                            let GetAllSongsData = await GetAllSongs(AllSongsIds);
                            for(let i = 0; i < result.length; i++){
                                let attemptData = result[i];
                                let getSongData = "";
                                let getSongId = AllAttemptSongsIds.find((item) => item.attempt_id == attemptData._id)
                                if(getSongId != undefined && getSongId != null && Object.keys(getSongId).length > 0 ){
                                    getSongData = GetAllSongsData.find((song) => song._id == getSongId.song_id )
                                }
                               

                                let cover_url = (attemptData.media != undefined && attemptData.media.cover_url != "" ) ? attemptData.media.cover_url : "";
                                let coverArr = (cover_url != "") ? cover_url.split("/") : [];
                                let cover_img = (coverArr.length > 0) ? coverArr[coverArr.length-1] : "";

                                let media_url = (attemptData.media != undefined && attemptData.media.public_media_url != "" ) ? attemptData.media.public_media_url : "";
                                let mediaUrlArr = (media_url != "") ? media_url.split("/") : [];
                                let public_media_url = (mediaUrlArr.length > 0) ? mediaUrlArr[mediaUrlArr.length-1] : "";

                                let statisticsObj = {
                                    "like_count": (attemptData.like_count) ? attemptData.like_count : 0,
                                    "play_count": (attemptData.play_count) ? attemptData.play_count : 0,
                                    "comment_count": (attemptData.comment_count) ? attemptData.comment_count : 0,
                                    "share_count": (attemptData.share_count) ? attemptData.share_count : 0,
                                    "clap_count": (attemptData.clap) ? attemptData.clap : 0,
                                    "view_count": (attemptData.viewsCount) ? attemptData.viewsCount : 0
                                }

                                let submits = {
                                    "attempt_id": attemptData._id,
                                    "score": (attemptData.score != undefined && attemptData.score != null && Object.keys(attemptData.score).length > 0 ) ? attemptData.score: {},
                                    "statistics" : statisticsObj,
                                    "attempt_time": attemptData.time,
                                    "song_id": (getSongData != undefined && getSongData != null && Object.keys(getSongData).length > 0 ) ? getSongData._id : "",
                                    "song_title": (getSongData != undefined && getSongData != null && Object.keys(getSongData).length > 0 ) ? getSongData.title : "",
                                    "song_data": getSongData,
                                    "comment": "",
                                    "cover_url": Url+cover_img,
                                    "public_media_url": Url+public_media_url
                                }
                                submitArr.push(submits)
                            }
                            
                            resolve(submitArr)
                        })();
                        
                    }else{
                        resolve([])
                    }
                }
            }
        )
    });
}

Cover.getCoverDraft = (req,res) => {
    let Url = "https://d286snw20yvi04.cloudfront.net/";
    if (!req.body.user_id) {
        return res
          .status(400)
          .send({ status: 400, message: "user_id is required!!" });
      }

      if (!req.body.type) {
        return res
          .status(400)
          .send({ status: 400, message: "type is required!!" });
      }
    let reqUserId = req.body.user_id
    let type = req.body.type
    let status
    if(type=='draft'){
        status=1
    }
    if(type=='cover'){
        status=3
    }
    return new Promise((resolve, reject) => {
        // attemptModel.find({user_id : reqUserId,status : status,'media.cover_url' : {$ne : ""}}).sort({time:-1}).lean().exec(
            attemptModel.find({user_id : reqUserId,status : status}).sort({time:-1}).lean().exec(
            function(err,result){
                if(err){
                    reject(err)
                }else{
                    if(result.length > 0){
                        (async function() {
                            let submitArr = [];
                            let AllAttemptSongsIds = await AttemptUploadModel.find({user_id:reqUserId}).select({song_id:1,attempt_id:1});
                            let AllSongsIds = AllAttemptSongsIds.map((song) => song.song_id)
                            let GetAllSongsData = await GetAllSongs(AllSongsIds);
                            for(let i = 0; i < result.length; i++){
                                let attemptData = result[i];
                                let getSongData = "";
                                let getSongId = AllAttemptSongsIds.find((item) => item.attempt_id == attemptData._id)
                                if(getSongId != undefined && getSongId != null && Object.keys(getSongId).length > 0 ){
                                    getSongData = GetAllSongsData.find((song) => song._id == getSongId.song_id )
                                }
                               

                                let cover_url = (attemptData.media != undefined && attemptData.media.cover_url != "" ) ? attemptData.media.cover_url : "";
                                let coverArr = (cover_url != "") ? cover_url.split("/") : [];
                                let cover_img = (coverArr.length > 0) ? coverArr[coverArr.length-1] : "";

                                let media_url = (attemptData.media != undefined && attemptData.media.public_media_url != "" ) ? attemptData.media.public_media_url : "";
                                let mediaUrlArr = (media_url != "") ? media_url.split("/") : [];
                                let public_media_url = (mediaUrlArr.length > 0) ? mediaUrlArr[mediaUrlArr.length-1] : "";

                                let statisticsObj = {
                                    "like_count": (attemptData.like_count) ? attemptData.like_count : 0,
                                    "play_count": (attemptData.play_count) ? attemptData.play_count : 0,
                                    "comment_count": (attemptData.comment_count) ? attemptData.comment_count : 0,
                                    "share_count": (attemptData.share_count) ? attemptData.share_count : 0,
                                    "clap_count": (attemptData.clap) ? attemptData.clap : 0,
                                    "view_count": (attemptData.viewsCount) ? attemptData.viewsCount : 0
                                }

                                let submits = {
                                    "attempt_id": attemptData._id,
                                    "score": (attemptData.score != undefined && attemptData.score != null && Object.keys(attemptData.score).length > 0 ) ? attemptData.score: {},
                                    "statistics" : statisticsObj,
                                    "attempt_time": attemptData.time,
                                    "song_id": (getSongData != undefined && getSongData != null && Object.keys(getSongData).length > 0 ) ? getSongData._id : "",
                                    "song_title": (getSongData != undefined && getSongData != null && Object.keys(getSongData).length > 0 ) ? getSongData.title : "",
                                    "comment": "",
                                    "cover_url": Url+cover_img,
                                    "public_media_url": Url+public_media_url
                                }
                                submitArr.push(submits)
                            }
                            
                            resolve(submitArr)
                        })();
                        
                    }else{
                        resolve([])
                    }
                }
            }
        )
    });
}

const GetAllSongs = (AllSongsIds) => {
    return new Promise((resolve, reject) => {
        SingModel.find( { _id : {$in :AllSongsIds} }).exec(
          function(err,result){
            if(err){
              reject(err)
            }else{
              if(result.length > 0){
                resolve(result)
              }else{
                resolve([])
              }
            }
          })
      })
}

Cover.downloadVideo = (req) => {
    return new Promise((resolve, reject) => {
      try {
        console.log("AttemptId: " + req.body.attempt_id);
        if (!req.body.attempt_id) {
          reject("Please enter attempt_id");
        }
        //let workDir = 'C:/Users/DELL/Downloads/video-test/';
        let workDir='/opt/downloadcover/';
        let mediaUrl = "";
        let lastPart = "";
        let secondLastPart = "";
        let thirdLastPart = "";
        
        (async function () {
          console.log("Log 1");
            let result = await fetchAttemptData(req.body.attempt_id);
            console.log("Log 2");
                (async function () {
                  console.log("Log 3");
                let userDetails =  await UserDetails(result.user_id);
                console.log("Log 5");
                console.log("UserId===" + userDetails[0]._id);
                console.log("Log 6");
                (async function () {
                console.log("UserId===" + userDetails[0].profile_img);
                 let profileUrl = String(userDetails[0].profile_img);
                await downloadImage(profileUrl,workDir,userDetails[0]._id);
                //await cropPicture(workDir+userDetails[0]._id+"_.png",workDir+userDetails[0]._id+".png")
                mediaUrl = result.media["media_url"];
                mediaUrl = String(mediaUrl);
                let parts = mediaUrl.split("/");
                lastPart = parts[parts.length - 1];
                secondLastPart = parts[parts.length - 2];
                thirdLastPart = parts[parts.length - 3];
                mediaUrl = thirdLastPart + "/" + secondLastPart + "/" + lastPart;
               
                (async function () {
                  await saveS3File(mediaUrl, workDir, lastPart);
                  var process = new ffmpeg(workDir + lastPart);
                  process.then(
                    function (video) {
                      //Trim the video
                      video.addCommand("-y", "");
                      video.setVideoStartTime(5);
                      video.setVideoDuration(17);
                      video.addCommand("-acodec", "copy");
                      video.save(
                        workDir + "trimmed_cover_" + lastPart,
                        function (error, file) {
                          if (!error) {
                            console.log("New video file: " + file);
                              var process = new ffmpeg(file);
                              process.then(
                                  function (video) {
                                  // let workDir1 =
                                  //     "C:/Users/DELL/Downloads/video-test/";
                                  let workDir1='/opt/downloadcover/';
                                  var fontFile =
                                      workDir1 + "bold_text.otf";
                                  var filter_complex =
                                      "[0]scale=720:1280:force_original_aspect_ratio=increase,crop=720:1280[v1];[v1][1]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)*0.9:enable='between(t,0.1,14)',drawtext=fontfile=" +
                                      fontFile +
                                      ":text='"+userDetails[0].user_handle+"':fontcolor=white:fontsize=26:box=1:boxcolor=black@0.5:boxborderw=5:x=(w-text_w)/2:y=(main_h-text_h)*0.891,fade=t=in:st=0:d=0.5[v2];[v2][2]overlay=0.1:0.1:enable='gt(t,14)'[v2];[v2][3]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)*0.24:enable='gt(t,14)',drawtext=fontfile=" +
                                      fontFile +
                                      ":text='"+userDetails[0].display_name+"':fontcolor=white:fontsize=30:shadowcolor=black:shadowx=5:shadowy=5:x=(w-text_w)/2:y=(main_h-text_h)*0.42:enable='gt(t,14)',fade=t=out:st=16.5:d=0.5,drawtext=fontfile=" +
                                      fontFile +
                                      ":text='"+userDetails[0].user_handle+"':fontcolor=white@0.5:fontsize=24:shadowcolor=black:shadowx=2:shadowy=2:x=(w-text_w)/2:y=(main_h-text_h)*0.45:enable='gt(t,14)',fade=t=out:st=16.5:d=0.5[v3]";
                                  console.log("Filter String " + filter_complex);
                                  video.addInput(
                                      workDir + "logo1.png"
                                  );
                                  video.addInput(
                                      workDir + "logo2.png"
                                  );
                               
                                  video.addInput(workDir+userDetails[0]._id+'.png'
                                   );
                                  video.addFilterComplex(filter_complex);
                                  video.addCommand("-map", "[v3]");
                                  video.addCommand("-map", "0:a");
                                  video.save(
                                      workDir + "cover_final_" + lastPart,
                                      function (error, file) {
                                      if (!error) {
                                          
                                          const fileContent =
                                          fs.readFileSync(file);
  
                                          const params = {
                                          Bucket: "singstr.user.content",
  
                                          Key:
                                              thirdLastPart +
                                              "/" +
                                              secondLastPart +
                                              "/" +
                                              "downloadable_cover_" +
                                              lastPart, 
                                          Body: fileContent,
                                          };
                                          s3.upload(
                                          params,
                                          function (s3Err, data) {
                                              if (s3Err) throw s3Err;
                                              console.log(
                                              `File uploaded successfully at ${data.Location}`
                                              );
                                              let params1 = {
                                                Bucket: 'singstr.user.content',
                                                Key:
                                                thirdLastPart +
                                                "/" +
                                                secondLastPart +
                                                "/" +
                                                "downloadable_cover_" +
                                                lastPart, 
                                                Expires: 60 * 5
                                              };
                                              s3.getSignedUrl('getObject', params1, function (err, url) {
                                                console.log("url="+url)
                                                cleanUpFileSystem(userDetails[0]._id,workDir,lastPart);
                                                resolve(url);
                                              });
                            
                                          }
                                          );
                                      } else {
                                          console.log("Error: " + error);
                                          reject(error);
                                      }
                                      },
                                      function (err) {
                                      console.log("Error: " + err);
                                      reject(err);
                                      }
                                  );
                                  },
                                  function (err) {
                                     console.log("Error: " + err);
                                     reject(err);
                                  }
                              );
                              
                          } else {
                              console.log("Error: " + error);
                              reject(error);
                          }
                        }
                      );
                    },
                    function (err) {
                      console.log("Error: " + err);
                    }
                  );
                 
               
                })();

                })();
                })();

              
        })();
      } catch (e) {
        console.log(e.code);
        console.log(e.msg);
        reject(e);
      }
    });
  };



let cleanUpFileSystem = (userId,workDir,mediaFilename)=>{

  fs.unlink(workDir+userId+'.png',function(err){
    if(err) return console.log(err);
   // console.log('file deleted successfully');
  }); 

  fs.unlink(workDir+mediaFilename,function(err){
    if(err) return console.log(err);
  //  console.log('file deleted successfully');
  }); 

  fs.unlink(workDir+'trimmed_cover_'+mediaFilename,function(err){
    if(err) return console.log(err);
   // console.log('file deleted successfully');
  }); 
  fs.unlink(workDir+'cover_final_'+mediaFilename,function(err){
    if(err) return console.log(err);
   // console.log('file deleted successfully');
  }); 

}

let fetchAttemptData = (attemptId)=>{
  return new Promise((resolve, reject) => {
    console.log("response attempt===");
  attemptModel.findOne(
    { _id: attemptId },
    function (err, response) {
      if (err) {
        reject(err);
      } else {
        console.log("response attempt===");
        resolve(response);
        
      }
    }
  );
  })
}

  let downloadImage =  (url, workDir,userId) => {
    return new Promise((resolve, reject) => {
      console.log("downloading image"+ url );
        if (url.includes("google")) {
          if (url.includes("=s")) {
            let parts = url.split("=");
            url = parts[0]+'=s175';

          }
        client.get(url, (res) => {
            if (res.statusCode === 200) {
                res.pipe(fs.createWriteStream(workDir+userId+".png"))
                    .on('error', reject)
                    .once('close', () => 
                   
                    resolve(workDir+userId+".png"));
            } else {
                // Consume response data to free up memory
                console.log("downloading image error" );
                res.resume();
                reject(new Error(`Request Failed With a Status Code: ${res.statusCode}`));

            }
        });
      } else  if (url.includes("s3") || url.includes("userProfile") ) {
        let parts = url.split("/");
        let lastPart = parts[parts.length - 1];
        let secondLastPart = parts[parts.length - 2];
        let thirdLastPart = parts[parts.length - 3];
        url = thirdLastPart + "/" + secondLastPart + "/" + lastPart;

        let params = {
          Bucket: "singstr.content",
          Key: url,
        };
        let readStream = s3.getObject(params).createReadStream();
        let writeStream = fs.createWriteStream(workDir+lastPart);
        readStream.pipe(writeStream);  
        writeStream.on('finish', function(){
            resolve(true)
        });
      }
    });
}
  
  let saveS3File = (mediaUrl, workDir, lastPart) => {
          return new Promise((resolve, reject) => {
              let params = {
                  Bucket: "singstr.user.content",
                  //Key: "61efcd02cc90df21bbb19cd2/615348841851801c1261d3a1/media_62061017cc90df2126351b1d.mp4",
                  Key: mediaUrl,
              };
              let readStream = s3.getObject(params).createReadStream();
              let writeStream = fs.createWriteStream(workDir + lastPart);
              readStream.pipe(writeStream);  
              writeStream.on('finish', function(){
                  resolve(true)
              });
          });
  
  };

  const UserDetails = (userId) => {
    return new Promise((resolve, reject) => {
        UserModel.find( { _id :userId }).select({_id: 1, first_name: 1, last_name: 1, profile_img: 1, display_name: 1,user_handle : 1}).exec(
          function(err,result){
            if(err){
              reject(err)
            }else{
              if(result.length > 0){
                console.log("Test.." + result);
                resolve(result)
              }else{
                resolve([])
              }
            }
          })
      })
}
  
Cover.getS3Object = (req) =>{
    return new Promise((resolve, reject) =>{
      if(!req.body.attempt_id){
        reject("Please enter attempt_id")
      }
      attemptModel.find( { _id : req.body.attempt_id}).exec(
        function(err,result){
          if(err){
            reject(err)
          }else{
            if(result.length > 0){
              let mediaUrl = result[0].media['media_url']
              mediaUrl = String(mediaUrl)
              console.log(typeof(mediaUrl))
              console.log("media==="+mediaUrl)

              let parts = mediaUrl.split("/");
              let lastPart = parts[parts.length - 1];
              let secondLastPart = parts[parts.length - 2];
              let thirdLastPart = parts[parts.length - 3];
              mediaUrl = thirdLastPart+'/'+ secondLastPart+'/'+lastPart
                let params = {
                    Bucket: 'singstr.user.content',
                    // Key: "61efcd02cc90df21bbb19cd2/615348841851801c1261d3a1/media_62061017cc90df2126351b1d.mp4",
                    Key: mediaUrl,
                    Expires: 60 * 5
                  };
                  s3.getSignedUrl('getObject', params, function (err, url) {
                    console.log("url="+url)
                    resolve(url)
                  });
            }else{
              resolve("Media Url not found")
            }
          }
        })
    //   var params = {
    //     Bucket: 'singstr.user.content',
    //     Key: "61efcd02cc90df21bbb19cd2/615348841851801c1261d3a1/media_62061017cc90df2126351b1d.mp4",
    //     Key: "61efcd02cc90df21bbb19cd2/615348841851801c1261d3a1/media_62061017cc90df2126351b1d.mp4",
    //     Expires: 60 * 5
    //   };
    //   s3.getSignedUrl('getObject', params, function (err, url) {
    //     console.log("url="+url)
    //   });
     })
  };

  Cover.updateCoverMediaUrlStatusForStaging = (req) =>{
    return new Promise((resolve, reject) =>{
      if(!req.body.attempt_id){
        reject("Please enter attempt_id")
      }
        console.log("attempt_id= "+req.body.attempt_id);
        (async function () {
         
          let result = await fetchAttemptData(req.body.attempt_id);
         // let mediaUrl = String(result.media["media_url"]);
        //  if(mediaUrl.length>0) {

          let coverUrl =  "https://singstr.user.content.streaming.s3.amazonaws.com/media_"+req.body.attempt_id+".0000001.jpeg";
          let publicMediaUrl = "https://singstr.user.content.streaming.s3.amazonaws.com/media_"+req.body.attempt_id+".m3u8";
    
          attemptModel.findOneAndUpdate(
            { _id: req.body.attempt_id },
            {
              $set: {                  
                  "media.public_media_url":publicMediaUrl,
                  "media.cover_url":coverUrl,
              },
            },
            { new: true },
            function (err, data) {
              if (err) {
                reject(err);
              } else {
                resolve("Attempt updated");
              }
            }
          );
          // } else {
          //   resolve("Attempt is not processed");
          // }

        })();
      })
};
  
module.exports = Cover