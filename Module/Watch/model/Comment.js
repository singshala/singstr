"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");

mongoose.set("debug", true);
let conWatch = mongoose.createConnection(config.mongo.watchUrl);
let conUser = mongoose.createConnection(config.mongo.userUrl);
let conNotifierUrl = mongoose.createConnection(config.mongo.notifierUrl);


let CommentReplySchema = require("../../../Schema/CommentReplySchema");
let UserSchema = require("../../../Schema/UserSchema");
let CommentSchema = require("../../../Schema/CommentSchema");
let NotificationSchema = require("../../../Schema/NotificationSchema");
const OnEventNotificationSchema = require("../../../Schema/OnEventNotificationSchema");
let AttemptSchema = require("../../../Schema/AttemptSchema");

let Comment = {};
const CommentReplyModel = conWatch.model("COMMENTREPLY",CommentReplySchema,"CommentReply");
const UserModel = conUser.model("USER", UserSchema, "User");
const CommentModel = conWatch.model("COMMENT",CommentSchema,"Comment");
const NotificationModel = conNotifierUrl.model("NOTIFICATION", NotificationSchema, "Notification");
const OnEventNotificationModel = conNotifierUrl.model("ONEVENTNOTIFICATION", OnEventNotificationSchema, "OnEventNotification");
const AttemptModel = conWatch.model("ATTEMPT",AttemptSchema,"Attempt");

Comment.AddReply = (req) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    return new Promise((resolve, reject) => {
        (async function() {
            let UserDetailsdata = await UserDetails(userId);
            let id = new mongoose.Types.ObjectId();
            let insertObj = {
                _id: id.toString(),
                attempt_id : req.body.attempt_id,
                comment_id : req.body.comment_id,
                reply : req.body.reply,
                time : new Date(),
                timestamp : Math.floor(new Date().getTime()/ 1000),
                user: {
                    id: UserDetailsdata[0]._id,
                    first_name: UserDetailsdata[0].first_name,
                    last_name:  UserDetailsdata[0].last_name,
                    profile_url: UserDetailsdata[0].profile_img,
                    display_name: UserDetailsdata[0].display_name,
                    user_handle : UserDetailsdata[0].user_handle
                }
            }
            
            CommentReplyModel.collection.insertOne(insertObj, function (err, response) {
                if(err){
                   reject(err)
                }else{
                  (async function() {
                    let getCommentUser = await CommentUserDetails(req.body.comment_id);
                    let GetNotificationEvent = await NotificationEventSave("CommentReply")
                    let notificationMsg = `${UserDetailsdata[0].first_name} ${UserDetailsdata[0].last_name} has replied to your comment`; 
                    let notificationData = {};
                  //  console.log("4", getCommentUser.id);
                  //  console.log("5", UserDetailsdata[0]._id);
                  //  console.log(getCommentUser.id.toString() != UserDetailsdata[0]._id.toString())

                   if(getCommentUser.id.toString() != UserDetailsdata[0]._id.toString()){
                    let notificationData = {
                      userId : getCommentUser.id,
                      message : Buffer.from(notificationMsg),
                      notificationEventId : GetNotificationEvent,
                      // deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments",
                      deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments/reply/"+req.body.comment_id,
                      icon_url : getCommentUser.profile_url
                    }
                    await NotificationSave(notificationData)
                   }

                    if(req.body.tag_user_id && req.body.tag_user_id.length>0){
                      for(let i=0;i<req.body.tag_user_id.length;i++){
                          let getTagUser = await UserDetails(req.body.tag_user_id[i]);
                        // console.log("tuid=",getTagUser)
                        if(getTagUser.length>0){
                          let GetNotificationEvent = await NotificationEventSave("CommentReplyTag")
                          let notificationMsg = `${UserDetailsdata[0].first_name} ${UserDetailsdata[0].last_name} has mention you in a comment`; 
                          let tagNotificationData = {
                            userId : getTagUser[0]._id,
                            message : Buffer.from(notificationMsg),
                            notificationEventId : GetNotificationEvent,
                            deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments/reply/"+req.body.comment_id,
                            icon_url : getTagUser[0].profile_img
                          }
                        
                          await NotificationSave(tagNotificationData)
                        }
                          
                      } 
                      
                    }
                    resolve("Reply added")
                  })()
                }
            });

           
        })();
    })

}
Comment.GetCommentById = (req) => {
  let commentId = req.params.CommentId;
  return new Promise((resolve,reject) => {
    CommentModel.find({_id: commentId}).lean().exec(
      function(err,result){
        if(err){
          reject(err)
        }else{
          if(result.length > 0){
            (async function(){
              const getReplyData = await CommentReplyModel.find({comment_id : commentId}).lean();
              let commentData = result.map((obj) => {
                let replyData = getReplyData;
                let TotalReply = (replyData.length > 0) ? replyData.length : 0;
                return obj = {...obj,TotalReply,replyData}
               
              })
              resolve(commentData)
            })()
          }else{
            resolve([])
          }
        }
      }
    )
  })

}
const UserDetails = (userId) => {
    return new Promise((resolve, reject) => {
        UserModel.find( { _id :userId }).select({_id: 1, first_name: 1, last_name: 1, profile_img: 1, display_name: 1,user_handle : 1}).exec(
          function(err,result){
            if(err){
              reject(err)
            }else{
              if(result.length > 0){
                resolve(result)
              }else{
                resolve([])
              }
            }
          })
      })
}
const CommentUserDetails = (commentId) => {
  return new Promise((resolve, reject) => {
    CommentModel.find({_id: commentId}).select({user:1}).lean().exec(
      function(err,result){
        if(err){
          reject(err)
        }else{
          if(result.length > 0){
            resolve(result[0].user)
          }else{
            resolve([])
          }
        }
      })
  })
}
const NotificationEventSave = (eventType) => {
  return new Promise((resolve, reject) => {
    OnEventNotificationModel.find({"on_events.name": eventType}).lean().exec(
      function(err,result){
        if(err){
          reject(err)
        }else{
          if(result.length > 0){
           resolve(result[0]._id)
          }else{
            let id = new mongoose.Types.ObjectId();
            let insertObj = {
                "_id" : id.toString(),
                "title": "Comment",
                "description": "",
                "send_count": 1,
                "status": 1,
                "notifier_data": {
                    "title": "Comment",
                    "text_template_name": "event-OnCoverComment-text.tmpl",
                    "html_template_name": "",
                    "app_image_url": "",
                    "app_button_txt": "",
                    "img_url": "",
                    "click_url": ""
                },
                "on_events": [{
                    "name": eventType
                }]
            }
            OnEventNotificationModel.collection.insertOne(insertObj, function (err, response) {
              if(err){
                 reject(err)
              }else{
                resolve(response.insertedId.toString())
              }
            });
           
          }
        }
      })
  })
}
const NotificationSave = (data) => {
  return new Promise((resolve, reject) => {
    let id = new mongoose.Types.ObjectId();
    let message = data.message;
    let insertObj = {
      "_id" : id.toString(),
      "group_id": "",
      "subscription_type": 1,
      "subscription_ids": [],
      "user_ids": [data.userId],
      "status": 1,
      "subject": "Comment",
      "data": message,
      "created_at" : new Date(),
      "updated_at" : new Date(),
      "send_by": 1,
      "segments": [],
      "initiated_by": 0,
      "dashboard_user_id": "",
      "on_event_notification_id": data.notificationEventId,
      "seen": false,
      "deep_link": data.deep_link,
      "icon_url": data.icon_url
      }
      NotificationModel.collection.insertOne(insertObj, function (err, response) {
        if(err){
          reject(err)
        }else{
          resolve(response.insertedId.toString())
        }
      });
  })
       
}

Comment.AddComment = (req) => {
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  // console.log(req.body.attempt_id)
  return new Promise((resolve, reject) => {
      (async function() {
          let UserDetailsdata = await UserDetails(userId);
          let AttemptDetailData = await AttemptDetail(req.body.attempt_id);
          let id = new mongoose.Types.ObjectId();
          let insertObj = {
              _id: id.toString(),
              attempt_id : req.body.attempt_id,
              comment : req.body.comment,
              time : new Date(),
              timestamp : Math.floor(new Date().getTime()/ 1000),
              user: {
                  id: UserDetailsdata[0]._id,
                  first_name: UserDetailsdata[0].first_name,
                  last_name:  UserDetailsdata[0].last_name,
                  profile_url: UserDetailsdata[0].profile_img,
                  display_name: UserDetailsdata[0].display_name,
                  user_handle : UserDetailsdata[0].user_handle
              }
          }

          if(req.body.tag_user_id && req.body.tag_user_id.length>0){
            insertObj.tag_user_id=req.body.tag_user_id;
          }
          
          CommentModel.collection.insertOne(insertObj, function (err, response) {
              if(err){
                 reject(err)
              }else{
                (async function() {
                //  let getCommentUser = await UserDetails(UserDetailsdata[0]._id);
                  let GetNotificationEvent = await NotificationEventSave("Comment")
                  let notificationMsg = `${UserDetailsdata[0].first_name} ${UserDetailsdata[0].last_name} has commented on your cover`; 
                  let notificationData = {}
                  //console.log("s", UserDetailsdata[0]._id.toString() != AttemptDetailData[0].user_id.toString())
                  // console.log("s1", UserDetailsdata[0]._id)
                  // console.log("s2", AttemptDetailData[0].user_id)
                 
                  if(UserDetailsdata[0]._id.toString() != AttemptDetailData[0].user_id.toString()){
                    let notificationData = {
                      userId : AttemptDetailData[0].user_id,
                      message : Buffer.from(notificationMsg),
                      notificationEventId : GetNotificationEvent,
                      deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments",
                      icon_url : UserDetailsdata[0].profile_img
                    }
                    await NotificationSave(notificationData)
                  }

                  if(req.body.tag_user_id && req.body.tag_user_id.length>0){
                    for(let i=0;i<req.body.tag_user_id.length;i++){
                        let getTagUser = await UserDetails(req.body.tag_user_id[i]);
                      // console.log("tuid=",getTagUser)
                      if(getTagUser.length>0){
                        let GetNotificationEvent = await NotificationEventSave("CommentTag")
                        let notificationMsg = `${UserDetailsdata[0].first_name} ${UserDetailsdata[0].last_name} has mention you in a comment`; 
                        let tagNotificationData = {
                          userId : getTagUser[0]._id,
                          message : Buffer.from(notificationMsg),
                          notificationEventId : GetNotificationEvent,
                          // deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments/reply/"+req.body.comment_id,
                          deep_link : "https://www.singshala.com/app/covers/"+req.body.attempt_id+"/comments",
                          icon_url : UserDetailsdata[0].profile_img
                        }
                      
                        await NotificationSave(tagNotificationData)
                      }
                        
                    } 
                    
                  }
                  resolve("Comment added")
                })()
              }
          });

         
      })();
  })

}

const AttemptDetail  = (attempt_id) => {
  return new Promise((resolve, reject) => {
      AttemptModel.find( { _id :attempt_id }).select({user_id: 1}).exec(
        function(err,result){
          if(err){
            reject(err)
          }else{
            if(result.length > 0){
              resolve(result)
            }else{
              resolve([])
            }
          }
        })
    })
}

module.exports = Comment;

