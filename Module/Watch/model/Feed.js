"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");


// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.userUrl);
let conWatch = mongoose.createConnection(config.mongo.watchUrl);
let conSing = mongoose.createConnection(config.mongo.singUrl);

const UserSchema = require("../../../Schema/UserSchema");
const attemptSchema = require("../../../Schema/AttemptSchema");
const UserSubsriberSchema = require("../../../Schema/UserSubsriberSchema");
const attempUploadSchema = require("../../../Schema/AttemptUploadSchema");
const SingSchema = require("../../../Schema/SingSchema");


let Feed = {};
const UserModel = conUser.model("USER", UserSchema, "User");
const attemptModel = conWatch.model("ATTEMPT",attemptSchema,"Attempt");
const UserSubsriberModel = conUser.model("USERSUBSRIBER", UserSubsriberSchema,"UserSubsriber");
const AttemptUploadModel = conWatch.model("ATTEMPTUPLOAD",attempUploadSchema,"AttemptUpload");
const SingModel = conSing.model("SING",SingSchema,"Sing");


Feed.getFollowingFeed = (req) => {
    let authorization = req.headers["authorization"].split(" ");
    var decoded = jwt_decode(authorization[1]);
    let userId = decoded.user_id;
    return new Promise((resolve, reject) => {
        UserSubsriberModel.distinct( "subscriber_id", { user_id: userId }).exec(
            function(err,result){
                if(err){
                reject(err)
                }else{
                    (async function() {
                        let finalData = [];
                        let next_page_token = 0;
                        let dataUser = await FetchUser(result);
                        for(let i = 0; i < result.length; i++){
                            let getData = await AttemptUploadModel.aggregate([
                                { $match: {user_id : result[i]} },
                                { $sample: { size: 1 } },
                                { $project: { attempt_id: 1,song_id : 1 } },
                              ])
                              //find({user_id : result[i]}).select({attempt_id:1,song_id:1}).lean().limit(1);
                           
                            if(getData.length > 0) {
                                getData = getData[0];
                                let attemptData = await attemptModel.findOne({_id : getData.attempt_id}).lean().limit(1);
                                if(attemptData != undefined && attemptData != null && Object.keys(attemptData).length > 0){
                                
                                    let cover_url = (attemptData.media.cover_url != "") ? attemptData.media.cover_url : "";
                                    let coverArr = (cover_url != "") ? cover_url.split("/") : [];
                                    let cover_img = (coverArr.length > 0) ? coverArr[coverArr.length-1] : "";
                                    next_page_token = attemptData.order_id;
                                    
                                    let attempt = {
                                        "id": attemptData._id,
                                        "order_id": attemptData.order_id,
                                        "cover_url": "https://d286snw20yvi04.cloudfront.net/"+cover_img,
                                        "time": attemptData.time,
                                        "score": (attemptData.score != undefined && attemptData.score != null && Object.keys(attemptData.score).length > 0 ) ? attemptData.score.total : 0,
                                        "liked": (attemptData.like_count == 0) ? false : true,
                                        "caption": attemptData.caption
                                    }
                                    let statistics = {
                                        "like_count": attemptData.like_count,
                                        "play_count": attemptData.play_count,
                                        "comment_count": attemptData.comment_count,
                                        "share_count": attemptData.share_count,
                                        "clap_count": attemptData.clap,
                                        "view_count": attemptData.viewsCount
                                    }
                                    let SongDetails = await SingModel.findOne({_id : getData.song_id}).lean();
                                    let song = "";
                                    if(SongDetails != undefined && SongDetails != null && Object.keys(SongDetails).length > 0 ){
                                        song = {
                                            "id": SongDetails._id,
                                            "title": SongDetails.title,
                                            "album": SongDetails.album,
                                            "year": SongDetails.year,
                                            "order": (SongDetails.sections != undefined && SongDetails.sections != null ) ? SongDetails.sections.order_no : 0,
                                            "lyrics": SongDetails.lyrics,
                                            "lyrics_start_time": SongDetails.lyrics_start_time,
                                            "artists": [
                                                {
                                                    "id": "125425544111225Scscs",
                                                    "name": (SongDetails.artists != undefined && SongDetails.artists != null && SongDetails.artists.length > 0) ? SongDetails.artists[0] : ""
                                                }
                                            ],
                                            "thumbnail_url": config.aws.cdn_prefix_url+"thumbnail/"+SongDetails.thumbnail_url,
                                            "difficulty": SongDetails.difficulty
                                        }
                                    }

                                    
                                    let getUser = dataUser.find((obj) => obj._id == attemptData.user_id);
                                    
                                    let author = (getUser != undefined && getUser != null && Object.keys(getUser).length > 0) ? getUser : "";
                                    
                                    let obj = {attempt,song,author,statistics,xp : 0}
                                    
                                    finalData.push(obj)
                                } 
                            }  
                            
                        }
                        //let finalArr = [];
                        let finalDataObj = {
                            submits : finalData,
                            next_page_token : next_page_token
                        }
                        //finalArr.push(finalDataObj)
                        resolve(finalDataObj)
                    })()
                }
            }
        )
    })
}
const FetchUser = (userIds) => {
    return new Promise((resolve, reject) => {
        UserModel.find( { _id : {$in :userIds} }).select({first_name:1,last_name:1,profile_img:1,user_handle:1}).exec(
          function(err,result){
            if(err){
              reject(err)
            }else{
              if(result.length > 0){
                resolve(result)
              }else{
                resolve([])
              }
            }
          })
      })
}
module.exports = Feed;