let Teacher = require("../model/Teacher");

exports.getTeacherDetails = function(req, res) {
    Teacher.getTeacherDetails(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.createTeacher = function(req, res) {
    Teacher.createTeacher(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.checkTeacher = function(req, res) {
    Teacher.checkTeacher(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};

exports.updateAttribute = function(req, res) {
    Teacher.updateAttribute(req)
    .then((result) => {
        let response = {}
        response = {
            status: 200,
            message: "SUCCESS",
            data: result,
        }
        res.status(200).send(response);
    }).catch((err) => {
        let response = {}
        response = {
            status: 404,
            message: "ERROR",
            error: err,
        }
        res.status(404).send(response);
    })
};
