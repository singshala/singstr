"user strict";
const mongoose = require("mongoose");
const config = require("../config.json");
let jwt_decode = require("jwt-decode");

// mongoose.Promises = global.Promises;
mongoose.set("debug", true);
let conUser = mongoose.createConnection(config.mongo.userUrl);
let conTeacher = mongoose.createConnection(config.mongo.teacherUrl);

const TeacherSchema = require("../../../Schema/TeacherSchema");
const TeacherSubcribeSchema = require("../../../Schema/TeacherSubcribeSchema");
const UserSchema = require("../../../Schema/UserSchema");
const OneRupeeCodeSchema = require("../../../Schema/OneRupeeCodeSchema");
const OneRupeeSubscriberSchema = require("../../../Schema/OneRupeeSubscriberSchema");
const UserAccountSchema = require("../../../Schema/UserAccountSchema");

let Teacher = {};
const TeacherModel = conTeacher.model("TEACHER", TeacherSchema, "teacher");
const TeacherSubcribeModel = conTeacher.model(
  "TEACHERSUBSCRIBE",
  TeacherSubcribeSchema,
  "teacherSubscribe"
);
const UserModel = conUser.model("USER", UserSchema, "User");
const OneRupeeCodeModel = conUser.model(
  "ONERUPEECODES",
  OneRupeeCodeSchema,
  "OneRupeeCodes"
);
const OneRupeeSubscriberModel = conUser.model(
  "ONERUPEESUBSCRIBERS",
  OneRupeeSubscriberSchema,
  "OneRupeeSubscribers"
);
const UserAccountModel = conUser.model(
  "USERACCOUNT",
  UserAccountSchema,
  "UserAccount"
);

Teacher.getTeacherDetails = (req) => {
  let teacherId = req.params.teacherId;
  let authorization = req.headers["authorization"].split(" ");
  var decoded = jwt_decode(authorization[1]);
  let userId = decoded.user_id;
  return new Promise((resolve, reject) => {
    TeacherModel.find({ _id: teacherId })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err);
        } else {
          if (result.length > 0) {
            (async function () {
              const getTeacherSubscribeData =
                await TeacherSubcribeModel.distinct("subscriber_id", {
                  teacher_id: result[0]._id,
                });
              let finalResult = result.map((obj) => {
                let IsLoginUserfollowers = false;
                if (getTeacherSubscribeData.indexOf(userId) >= 0) {
                  IsLoginUserfollowers = true;
                }
                obj.profileImgUrl =
                  obj.profileImgUrl != ""
                    ? config.aws.cdn_prefix_url + obj.profileImgUrl
                    : "";
                return (obj = { ...obj, IsLoginUserfollowers });
              });

              resolve(finalResult);
            })();
          }
        }
      });
  });
};

let findUser = (findCase) => {
  return new Promise((resolve, reject) => {
    UserModel.find(findCase, function (err, response) {
      if (err) {
        reject(err);
      } else {
        resolve(response);
      }
    });
  });
};

Teacher.createTeacher = (req) => {
  let userId = req.params.userId;
  let findcase = { _id: userId };
  let findcase1 = { user_id: userId };
  return new Promise((resolve, reject) => {
    TeacherModel.find(findcase1, function (err, response) {
      if (err) {
        reject(err);
      } else {
        if (response.length > 0) {
          reject("user already a teacher");
        } else {
          (async function () {
            let response = await findUser(findcase);
            if (response.length > 0) {
              UserModel.findOneAndUpdate(
                { _id: req.params.userId },
                {
                  isTeacher: true,
                },
                { new: true },
                function (err, response) {
                  if (err) {
                    reject(err);
                  } else {
                    (async function () {
                      let result = await findUser(findcase);
                      if (result.length > 0) {
                        (async function () {
                          let details = [];
                          details.push({ sub_title: "", title: "" });

                          let attributes = [];
                          attributes.push({
                            details: details,
                            title: "",
                            order: "",
                            image: "",
                          });
                          let id = new mongoose.Types.ObjectId();
                          let insertObj = {
                            _id: id.toString(),
                            user_id: result[0]._id.toString(),
                            bio: result[0].bio,
                            name:
                              result[0].first_name + " " + result[0].last_name,
                            profileImgUrl: result[0].profile_img,
                            experience: "",
                            attributes: attributes,
                          };

                          TeacherModel.collection.insertOne(
                            insertObj,
                            function (err, response) {
                              if (err) {
                                reject("err");
                              } else {
                                //start
                                (async function () {
                                  let result = await findUser(findcase);
                                  if (result.length > 0) {
                                    (async function () {
                                      let date = new Date();
                                      let year = date.getFullYear();
                                      let month = date.getMonth();
                                      let day = date.getDate();
                                      let newDate = new Date(
                                        year + 60,
                                        month,
                                        day
                                      );
                                      let subscriptionNew = {};
                                      subscriptionNew = {
                                        validity: newDate,
                                      };
                                      // let insertObj = {
                                      //   _id: result[0]._id.toString(),
                                      //   credit: {},
                                      //   subscription: subscriptionNew,
                                      //   stats: {},
                                      // };
                                      let findcase3 = {
                                        _id: req.params.userId,
                                      };
                                      UserAccountModel.find(
                                        findcase3,
                                        function (err, response) {
                                          if (err) {
                                            reject(error);
                                          } else {
                                            if (response.length > 0) {
                                              UserAccountModel.findOneAndUpdate(
                                                {
                                                  _id: req.params.userId,
                                                },
                                                {
                                                  subscription: subscriptionNew,
                                                },
                                                { new: true },
                                                function (err, response) {
                                                  if (err) {
                                                    reject(err);
                                                  } else {
                                                    resolve("success");
                                                  }
                                                }
                                              );
                                            } else {
                                              reject(
                                                "No subscription details available"
                                              );
                                              // UserAccountModel.collection.insertOne(
                                              //   insertObj,
                                              //   function (err, response) {
                                              //     if (err) {
                                              //       reject("err");
                                              //     } else {
                                              //       resolve("success");
                                              //     }
                                              //   }
                                              // );
                                            }
                                          }
                                        }
                                      );
                                    })();
                                  } else {
                                    reject("user not found");
                                  }
                                })();
                                //end
                                // resolve(insertObj);
                              }
                            }
                          );
                        })();
                      } else {
                        reject("user not found");
                      }
                    })();
                  }
                }
              );
            } else {
              reject("user not found");
            }
          })();
        }
      }
    });
  });
};

Teacher.checkTeacher = (req) => {
  let user_id = req.query.user_id;
  let findcase = { _id: user_id };
  let findcase2 = { user_id: user_id };
  //let findcase3 = {isTeacher:{$exists: true}}
  return new Promise((resolve, reject) => {
    (async function () {
      let response = await findUser(findcase);
      if (response.length > 0) {
        //console.log("hello",isEmpty(response[0].isTeacher))
        // console.log(typeof response[0].isTeacher);
        if (response[0].isTeacher == "true") {
          TeacherModel.find(findcase2, function (err, result) {
            if (err) {
              reject(err);
            } else {
              if (result.length > 0) {
                resolve(result[0]._id);
              } else {
                resolve("false");
              }
            }
          });
        } else {
          resolve("false");
        }
      } else {
        reject("user not available");
      }
    })();
  });
};

Teacher.updateAttribute = (req) => {
  let teacherId = req.body.id;
  findCase = { _id: teacherId };

  return new Promise((resolve, reject) => {
    (async function () {
      let response = await attributeDetails(req);
      console.log(response);

      if (response.length > 0) {
        TeacherModel.findOneAndUpdate(
          { _id: teacherId },{
            $set: {
              attributes: response,
            },
          },
          

          { new: true },
          function (err, data) {
            if (err) {
              reject(err);
            } else {
              //resolve("User updated");
              resolve(data);
            }
          }
        );
      } else {
        reject("no teacher found");
      }
    })();
  });
};

let attributeDetails = (req) => {
  let teacherId = req.body.id;
  findCase = { _id: teacherId};
  return new Promise((resolve, reject) => {
    console.log("abc")
    TeacherModel.find(findCase, function (err, response) {
      if (err) {
        reject(err);
      } else {
        let attributes = [];
        let detailedAttr = []

        req.body.attributes.map(item => {
          item.details.map(data => {
            detailedAttr.push({sub_title: data.sub_title, title: data.title})
          })
        })

        req.body.attributes.map(item => {
          attributes.push({title: item.title, order: item.order,image: item.image, details: detailedAttr})

        })
       console.log(attributes)
        resolve(attributes);
      }
    });
  });
};

module.exports = Teacher;
