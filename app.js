const express = require('express')
const bodyParser = require('body-parser')
var http = require('http');
const dotenv = require('dotenv');
var routes = require('./route/approute'); //importing route
//Initializing dotenv file
dotenv.config();
const port = process.env.PORT;
const app = express()
var multer = require('multer');
var upload = multer();

app.use(express.urlencoded({ extended: true })); 

app.use(upload.array()); 
//app.use(express.static('public'));


http.createServer(app)
    .listen(port, function () {
        console.log('app listening on port '+port)
    })
//CORS Middleware
app.use(function (req, res, next) {
    //Enabling CORS
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT,DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-client-key, x-client-token, x-client-secret, Authorization");
    next();
    return;
});


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());



routes(app); //register the route