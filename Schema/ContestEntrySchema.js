const { ObjectId } = require("bson");
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let ContestEntrySchema = new Schema(
    {
    _id: { type: String },
    contestId:  { type: String },
    attemptId:  { type: String },
    userId:  { type: String },
    date: { type: Date },
    votes: [],
    date: { type: Date },
    isWinner: { type: Boolean },
    isValid: { type: Boolean },
    attempt:{}
});

module.exports = ContestEntrySchema
