const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AwardXpLeaderSchema = new Schema(
  
{
    _id : {Type:String},
    userid : {Type:String},
    firstName : {Type:String},
    lastName : {Type:String},
    email : {type:String},
    gender : {Type : Number},
    age : {Type : Number},
    curxp : {Type : Number},
    curlevel : {Type : Number},
    rank : {Type : Number},
    xptime : {Type : Date},
    

});

module.exports = AwardXpLeaderSchema

