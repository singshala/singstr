
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let CoverWatchHistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    attempt_id : {Type:String},
    time_in_seconds : {Type:Number},
   
});

module.exports = CoverWatchHistorySchema
