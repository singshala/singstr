const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let HistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    time : {Type:Date},
    attempt : {}
   
});

module.exports = HistorySchema
