

const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AttemptSchema = new Schema(
  
{
    _id : {Type:String},
    order_id : {Type:String},
    email_id : {Type:String},
    user_id : {Type:String},
    status : {Type : Number},
    reference_id : {Type:String},
    media :  {
        media_url: { Type:String },
        media_hash: { Type:String },
        metadata_url: { Type:String },
        metadata_hash: { Type:String },
        raw_recording_url: { Type:String},
        raw_recording_hash: {Type:String},
        cover_url: {Type:String},
        public_media_url : {Type:String}
    },
    contestEntry : {
        contestId:  { type: String },
        attemptId:  { type: String },
        userId:  { type: String },
        date: { type: Date },
    },
    isContest : {Type:Boolean}
    
});


module.exports = AttemptSchema


