const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let UserSchema = new Schema(
    /** 
* Paste one or more documents here
*/
{
    _id : {Type:String},
    login_type: {Type:Number},
    social_id: {Type:String},
    email: {Type:String},
    password: {Type:String},
    first_name: {Type:String},
    last_name: {Type:String},
    artist_name: {Type:String},
    contact_number: {Type:String},
    profile_img: {Type:String},
    extra: {},
    status : {Type:Number},
    devices: [],
    user_handle: {Type:String},
    bio: {Type:String},
    city: {Type:String},
    display_name: {Type:String},
    sex: {Type:Number},
    onboard_status: {Type:Boolean},
    singer_type: {Type:String},
    isTeacher:{Type:Boolean},
});

module.exports = UserSchema
