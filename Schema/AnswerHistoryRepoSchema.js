const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AnswerHistoryRepoSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    question_id : {Type:String},
    answer_id : {Type:String},
    is_correct : {type:Boolean},
    time : {Type : Date}
   

});

module.exports = AnswerHistoryRepoSchema
