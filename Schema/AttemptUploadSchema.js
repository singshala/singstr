
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AttemptUploadSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    attempt_id : {Type:String},
    song_id : {Type:String},
    status : {Type:Number},
   
});

module.exports = AttemptUploadSchema
