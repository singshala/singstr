const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let UnSubsriberHistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    time : {Type:Date},
    subscriber_id :{Type:String}   
});

module.exports = UnSubsriberHistorySchema
