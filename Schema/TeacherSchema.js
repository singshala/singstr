const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;




let TeacherSchema = new Schema(
  {
    _id: { type: String },
    user_id: { type: String },
    bio: { type: String },
    name: { type: String },
    profileImgUrl: { type: String },
    experience: { type: String },
    attributes: [],
  
});




module.exports = TeacherSchema