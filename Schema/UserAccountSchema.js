const { Double } = require("bson");
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let UserAccountSchema = new Schema(
  /**
   * Paste one or more documents here
   */
  {
    _id: { Type: String },
    credit: { Type: String },
    subscription : {},
    stats: {},
  }
);

module.exports = UserAccountSchema;
