const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;




let LevelSchema = new Schema(
  {
  _id: { type: String },
  school_id: {type:Schema.Types.ObjectId},
  stage_id: {type:Schema.Types.ObjectId},
  grade_id: {type:String},
  name: { type: String },
  display_name: { type: String },
  order: { type: String },
  description:{ type: String },
  stage_order: { type: String },
  grade_order: { type: String },
  subscription_purchase_type: { type: String },
  warm_up_exercises: {type:Array},
  popular_songs: {
    count: { type: String },
    progression_score: { type: String },
    sections_mandatory: {type:Boolean},
    sections_scorable: {type:Boolean},
    section_progression_score: { type: String },
    song_ids: {type:Array},
  },
  progression_steps: {
    guest: { type: String },
    hobby: { type: String },
    academic: { type: String },
  },
  video_id: { type: String },
  difficulty: { type: String },
  teacher_id: { type: String },
  primary_tag: { type: String },
  tags: {type:Array},
  lessons: [
    {
      _id: { type: String },
      name: { type: String },
      order: { type: String },
      exercises: [
        {
          _id: { type: String },
          school_id: { type: String },
          tags: {type:Array},
          reference_id: { type: String },
          name: { type: String },
          subtitle: { type: String },
          attributes: {
            music_notation_file_url: { type: String },
            image_url: { type: String },
            video_url: { type: String },
            video_subtitle_file_url: { type: String },
            audios: {type:Array},
          },
          status: {type:Array},
          data_files: [
            {
              version: { type: String },
              gender: { type: String },
              status: { type: String },
              processing_start_time: {
                $date: { type: String },
              },
              media_url: { type: String },
              media_hash: { type: String },
              media_license_expires_at: {
                $date: { type: String },
              },
              media_license_version: { type: String },
              metadata_url: { type: String },
              metadata_hash: { type: String },
              metadata_license_expires_at: {
                $date: { type: String },
              },
              metadata_license_version: { type: String },
            },
          ],
          order: { type: String },
          is_mandatory: {type:Boolean},
          is_scorable: {type:Boolean},
          progression_score: { type: String },
        }
      ],
      theories: {type:Array},
      ear_trainings: {type:Array},
      sight_reading_groups: {type:Array},
    },
  ],
  published: {type:Boolean},
  course_required_songs: {
    count: { type: String },
    progression_score: { type: String },
    sections_mandatory: {type:Boolean},
    sections_scorable: {type:Boolean},
    section_progression_score: { type: String },
    song_ids: {type:Array},
  },
  next_lesson_id: { type: String },
});




module.exports = LevelSchema