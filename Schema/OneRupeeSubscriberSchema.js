const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let OneRupeeSubscriberSchema = new Schema(
  
{
    _id : {Type:String},
    userId : {Type:String},
    assign : {type:Boolean},
    createdAt : {Type : Date}

});

module.exports = OneRupeeSubscriberSchema