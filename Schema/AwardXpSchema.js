const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AwardXpSchema = new Schema(
  
{
    _id : {Type:String},
    song_id : {Type:String},
    type : {Type:String},
    user_id : {Type:String},
    xp_type : {type:String},
    difficulty : {Type : String},
    song_xp : {Type : Number},
    Attempt_id : {Type : String}

});

module.exports = AwardXpSchema

