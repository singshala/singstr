const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let NotificationSchema = new Schema(
  
{
    _id : {Type:String},
    group_id : {Type:String},
    subscription_type : {Type:Number},
    subscription_ids : [],
    user_ids : [],
    status : {Type:Number},
    subject : {Type:String},
    data: {Type:Buffer},
    created_at : {Type:Date},
    updated_at : {Type:Date},
    send_by : {Type:Number},
    segments : [],
    initiated_by : {Type:Number},
    dashboard_user_id : {Type:String},
    on_event_notification_id : {Type:String}
   
},{strict:true});

// NotificationSchema.virtual('_id').get(function() {
//     return this._id.toString();
// });

module.exports = NotificationSchema

