const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;


let GradeSchema = new Schema(
  {
  _id: { type: String },
  concepts: [
    {
      concept_id: { type: String },
      
    },
  ],
  published :{type:Boolean}
},{
    strict: false
});


module.exports = GradeSchema