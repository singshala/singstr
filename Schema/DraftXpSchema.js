const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let AwardXpLeaderSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    song_id : {Type:String},
    draftAttempt_id : {Type:String},
    draftXp : {type:Number},
    draftState : {Type : Number},
    type : {Type : String},
    

});

module.exports = AwardXpLeaderSchema

