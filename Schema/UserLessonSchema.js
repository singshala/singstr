const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let UserLessonSchema = new Schema(
  
{
    _id : {Type:String},
    user_id :  {Type:String},
    lesson_id :  {Type:String},
    time : {Type:Date}

});

module.exports = UserLessonSchema
