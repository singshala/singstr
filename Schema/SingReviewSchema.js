const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let SingReviewSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    order_id : {Type:String},
    reference_id : {Type:String},
    time :{Type:Date}   
});

module.exports = SingReviewSchema

