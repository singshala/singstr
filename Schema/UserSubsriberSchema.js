const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let UserSubsriberSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    subscriber_id : {type:String},
    time : {Type : Date}

});

module.exports = UserSubsriberSchema

