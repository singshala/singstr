const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let CommentReplySchema = new Schema(
  
{
    _id : {Type:String},
    attempt_id : {Type:String},
    comment_id : {Type:String},
    reply : {Type:String},
    time : {Type:Date},
    timestamp : {Type: Number},
    user: {
        id: { type: String },
        first_name: { type: String },
        last_name: {type:String},
        profile_url: {type:String},
        display_name: { type: String },
      },
});


module.exports = CommentReplySchema

