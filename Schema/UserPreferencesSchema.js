

const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let UserPreferencesSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    song_type : [],
    preferred_language : [],
    time :{Type:Date}   
});

module.exports = UserPreferencesSchema

