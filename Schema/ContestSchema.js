const { ObjectId } = require("bson");
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let ContestSchema = new Schema(
    {
    _id: { type: String },
    ContestName:  { type: String },
    StartDate: { type: Date },
    SubmissionEndDate: { type: Date },
    WinnerAnnouncementDate:  { type: Date },
    DashboardBannerContents: { type: String },
    EligibilityLevel: { type: Number },
    HostName: { type: String },
    HostLogo: { type: String },
    ContestType: { type: String },
    ContestVideo: { type: String },
    Prize: { type: String },
    HowToEnter: { type: String },
    Rules: { type: String },
    JudgingCriteria:  { type: String },
    Winner:  {
        attemptId: {type: String}
    }
});

module.exports = ContestSchema
