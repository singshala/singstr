const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let VideoUploadHistorySchema = new Schema(
  
{
    _id : {Type:String},
    videoId : {Type:String},
    fileName : {type:String},
    thumbnail : {type:String},
    s3outPath : {type:String},
    checksum : {type:String},
    uploadStatus : {type:String},
    updatetime : {type:Date},
    lastUpdated : {type:Date},
    publicUrl : {type : String}
   

});

module.exports = VideoUploadHistorySchema

