const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let VideoSchema = new Schema(
  
{
    _id : {Type:String},
    video_url : {Type:String},
    thumbnail_url : {type:String},
    durration : {type:String},
    name : {type:String},
    description : {type:String},
    duration : {type:String},
    published : {type:Boolean},
    tags : {type:Array},
    public_url : {type:String}
   

});

module.exports = VideoSchema

