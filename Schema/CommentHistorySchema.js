
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let CommentHistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    attempt_id : {Type:String},
    deleted_by : {Type:String},
   
});

module.exports = CommentHistorySchema


