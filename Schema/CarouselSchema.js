const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let CarouselSchema = new Schema(
  
{
    _id : {Type:String},
    thumbnail_url : {Type:String},
    deeplink_url : {Type:String},
    order : {Type:Number},
   status : {Type:Boolean}

});


module.exports = CarouselSchema


