const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let LevelWatchedSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    level_id : {Type:String},
    video_id : {Type:String},
   
});

module.exports = LevelWatchedSchema

