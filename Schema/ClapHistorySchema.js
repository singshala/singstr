
const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let ClapHistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    attempt_id : {Type:String},
    time : {Type:Date},
   
});

module.exports = ClapHistorySchema


