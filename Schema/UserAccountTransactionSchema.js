

const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let UserAccountTransactionSchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    order : {},
    status :{Type:Number}   
});

module.exports = UserAccountTransactionSchema

