const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let OneRupeeCodeSchema = new Schema(
{
   code: { Type: String },
   createdAt : {Type : Date}
});

module.exports = OneRupeeCodeSchema