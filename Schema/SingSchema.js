const mongoose = require("mongoose");
mongoose.Promises = global.Promises;

const Schema = mongoose.Schema;

let SingSchema = new Schema(
    {
    _id: { type: String },
    order: 0,
    title:  { type: String },
    album: { type: String },
    year: { type: String },
    language:  { type: String },
    artists: { type: Array },
    genres: { type: Array },
    duration: { type: String },
    difficulty: { type: String },
    thumbnail_url: { type: String },
    preview_url: { type: String },
    media_url: { type: String },
    is_avail: { type: Boolean },
    video_url: { type: String },
    learn_mode_available:  { type: Boolean },
    lyrics: { type: String },
    lyrics_start_time:  { type: String },
    school_id: { type: String },
    school_name:  { type: String }
});

module.exports = SingSchema
