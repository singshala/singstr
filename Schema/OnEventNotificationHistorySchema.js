const mongoose = require("mongoose");
mongoose.Promises = global.Promises;


const Schema = mongoose.Schema;

let OnEventNotificationHistorySchema = new Schema(
  
{
    _id : {Type:String},
    user_id : {Type:String},
    on_event_notification_id : {Type:String},
    time : {Type:Date},
    status : {Type:Number},

});

module.exports = OnEventNotificationHistorySchema

