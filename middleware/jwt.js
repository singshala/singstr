let jwt_decode = require("jwt-decode");
let http = require("http");
exports.validJWTNeeded = (req, response, next) => {
  //return res.status(400).json({data:req.headers});
  if (req.headers["authorization"]) {
    try {
      let authorization = req.headers["authorization"].split(" ");
      if (authorization[0] !== "Bearer") {
        return response.status(401).send("pass token with Bearer");
      } else {
        // req.jwt = jwt.verify(authorization[1], secret);
        //return res.status(400).json({data1:req.jwt});
        (async function () {
          try {
            // var decoded = jwt_decode(authorization[1]);
            // console.log("decoded===",decoded)
            // let userId = decoded.user_id;
            var options = {
              host: 'localhost',
              port: 3000,
              path: '/v1.0/verify/user/'+ authorization[1],
              method: 'GET',
              headers: {
                  'Content-Type': 'application/json',
              },
            };
            http
              .get(options, (res) => {
                let data = [];
                res.on("data", (chunk) => {
                  data.push(chunk);
                });

                res.on("end", () => {
                  const finalResp = JSON.parse(Buffer.concat(data).toString());
                  if(finalResp.status==200){
                    return next();
                  }else{
                    return response.status(403).send({ error: "Invalid token" });
                  }
                });
              })
              .on("error", (err) => {
                return response.status(403).send({ error: err.message });
              });
          } catch (err) {
                return response.status(403).send({ error: "Invalid token" });
          }
        })();
      }
    } catch (err) {
      return response.status(403).send({ error: err });
    }
  } else {
    return response.status(401).send({ error: "you aren't authorized!!" });
  }
};
